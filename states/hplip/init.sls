# ---------------------------------------------------------------------------
# PRINTER
# ---------------------------------------------------------------------------
# sudo pacman -S cups
# sudo systemctl enabe cups-browsed.service
# sudo systemctl start cups-browsed.service
#
# sudo pacman -Syu
# sudo pacman -S hplip
#
# sudo usermod -aG lp uri
# sudo usermod -aG lp cups
#
# # LIST MODELS
# grep 476 /usr/share/hplip/data/models/models.dat
#
# lpstat -p -d
# lpadmin -p hplj -E -v ipp://192.168.88.50/ipp/print -m everywhere
# lpadmin -x Hewlett-Packard_HP_Color_LaserJet_MFP_M476dw
# lpinfo -m | grep 476
# lpstat -t
#
# # Manage Printer and Help
# http://localhost:631/admin
#
# # DEBUG
# sudo cupsctl --debug-logging
# sudo less /var/log/cups/error_log
# sudo cupsctl --no-debug-logging
#
# # TROUBLESHOOTING HPLIP
# https://developers.hp.com/hp-linux-imaging-and-printing/KnowledgeBase/Troubleshooting/BasicHPLIPTroubleshooting
#
# # HELP AVAHI
# https://wiki.archlinux.org/index.php/Avahi
#
# # HP SETUP
# http://www.mygnulinux.com/?p=1192
# https://developers.hp.com/hp-linux-imaging-and-printing/install/install/index
# https://wiki.archlinux.org/index.php/CUPS/Printer-specific_problems#HPLIP
# https://wiki.archlinux.org/index.php/CUPS/Troubleshooting#HP_issues
# https://unix.stackexchange.com/questions/359531/installing-hp-printer-driver-for-arch-linux

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
    - name: {{ slspath }}


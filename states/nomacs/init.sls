{% set user = grains.user %}

{{ tplfile }}> Clone {{ slspath }}:
  git.cloned:
  - name: https://aur.archlinux.org/{{ slspath }}.git
  - user: {{ user }}
  - target: /home/{{ user }}/.local/sources/{{ slspath }}
  - creates: /home/{{ user }}/.local/sources/{{ slspath }}

{{ tplfile }}> Make and install package {{ slspath }}:
  cmd.run:
  - name: makepkg -sicC --noconfirm
  - runas: {{ user }}
  - cwd: /home/{{ user }}/.local/sources/{{ slspath }}
  - require:
    - {{ tplfile }}> Clone {{ slspath }}
  - onlyif: test -d '/home/{{ user }}/.local/sources/{{ slspath }}'
  - unless: which nomacs &> /dev/null

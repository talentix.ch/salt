{% if grains.id == 'clt-dsk-v-7133' %}
{{ tplfile }}> Mount backup volume:
  mount.mounted:
  - name: /backup
  - device: /dev/mapper/backup
  - fstype: xfs
  - opts: rw,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota
  - dump: 0
  - pass_num: 0
  - persist: True
  - mkmnt: True
  - failhard: True

# {{ tplfile }}> Mount backup volume:
#   mount.mounted:
#     - name: /backup
#     - device: UUID='fa825c74-24de-49ce-baa5-e3262359f72f'
#     - fstype: xfs
#     - opts: rw,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota
#     - dump: 0
#     - pass_num: 0
#     - persist: True
#     - mkmnt: True
#     - failhard: True
# 
# {{ tplfile }}> Mount backup volume:
#   file.managed:
#     - name: /etc/crypttab
#     - source: salt://{{ slspath }}/files/etc/crypttab
#     - require:
#       - {{ tplfile }}> Mount backup volume
{% endif %}

{% if grains.id == 'talentix' %}
{% set backup_device = '/dev/sda1' %}
# salt-call disk.blkid /dev/sda1 --out json | jq '.local."/dev/sda1".UUID' 
{% set uuid = salt.disk.blkid(backup_device)[backup_device]['UUID'] %}
{{ tplfile }}> Mount backup volume:
  mount.mounted:
  - name: /backup
  - device: UUID={{ uuid }}
  - fstype: xfs
  - opts: rw,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota
  - dump: 0
  - pass_num: 0
  - persist: True
  - mkmnt: True
  - failhard: True
{% endif %}

{% if grains.manufacturer == 'QEMU' %}

{{ tplfile }}> Mount host backup path added as guest filesystem tagged with name backup on /backup:
  mount.mounted:
  - name: /backup
  - device: backup
  - fstype: virtiofs
  - opts: rw,relatime
  - persist: False
  - mkmnt: True
  - failhard: True

# Mount host device as backup volume
# {# {% set backup_device = '/dev/vdb' %}                                       #} 
# {# # salt-call disk.blkid /dev/sda1 --out json | jq '.local."/dev/sda1".UUID' #} 
# {# {% set uuid = salt.disk.blkid(backup_device)[backup_device]['UUID'] %}     #}

# {{ tplfile }}> Mount host device as backup volume:
#   mount.mounted:
#     - name: /backup
#{#     - device: UUID={{ uuid }} #}
#     - fstype: xfs
#     - opts: ro,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota
#     - dump: 0
#     - pass_num: 0
#     - persist: False
#     - mkmnt: True
#     - failhard: True
{% endif %}

{% set user = grains.user %}

{{ tplfile }}> Clone {{ slspath }}:
  git.cloned:
  - name: https://aur.archlinux.org/{{ slspath }}.git
  - user: {{ user }}
  - target: /home/{{ user }}/.local/sources/{{ slspath }}
  - creates: /home/{{ user }}/.local/sources/{{ slspath }}

{{ tplfile }}> Make and install package {{ slspath }}:
  cmd.run:
  - name: makepkg -sicC --noconfirm
  - runas: {{ user }}
  - cwd: /home/{{ user }}/.local/sources/{{ slspath }}
  - require:
    - {{ tplfile }}> Clone {{ slspath }}
  - onlyif: test -d '/home/{{ user }}/.local/sources/{{ slspath }}'
  - unless: test -f /usr/bin/{{ slspath }}

{{ tplfile }}> Restore {{ slspath }} configuration for user {{ user }}:
  cmd.run:
  - name: rsync -a /backup/restore-user/.config/{{ slspath }} /home/{{ user }}/.config/
  - onlyif: test -d /backup/restore-user/.config/{{ slspath }}
  - unless: test -d /home/{{ user }}/.config/{{ slspath }}
  - require:
    - {{ tplfile }}> Make and install package {{ slspath }}
  - failhard: True

{{ tplfile }}> Configure 1-st backup schedule user {{ user }}:
  cron.present:
  - name: /usr/bin/nice -n19 /usr/bin/ionice -c2 -n7 /usr/bin/backintime backup-job >/dev/null
  - user: {{ user }}
  - minute: 0

{{ tplfile }}> Configure 2-nd backup schedule user {{ user }}:
  cron.present:
  - name: /usr/bin/nice -n19 /usr/bin/ionice -c2 -n7 /usr/bin/backintime --profile-id 2 backup-job >/dev/null
  - user: {{ user }}
  - minute: 15

{{ tplfile }}> Delete /tmp/backintime.lock-File:
  file.absent:
  - name: /tmp/backintime.lock

# sudo pkexec backintime-qt
{{ tplfile }}> Restore {{ slspath }} configuration for user root:
  cmd.run:
  - name: rsync -a /backup/restore-root/.config/{{ slspath }} /root/.config/
  - onlyif: test -d /backup/restore-root/.config/{{ slspath }}
  - unless: test -d /root/.config/{{ slspath }}
  - require:
    - {{ tplfile }}> Make and install package {{ slspath }}
  - failhard: True

{{ tplfile }}> Configure 1-st backup schedule user root:
  cron.present:
  - name: /usr/bin/nice -n19 /usr/bin/ionice -c2 -n7 /usr/bin/backintime backup-job >/dev/null
  - user: root
  - minute: 30

{{ tplfile }}> Configure 2-nd backup schedule user root:
  cron.present:
  - name: /usr/bin/nice -n19 /usr/bin/ionice -c2 -n7 /usr/bin/backintime --profile-id 2 backup-job >/dev/null
  - user: root
  - minute: 45

# {{ tplfile }}> Delete /tmp/backintime.lock-File for user root:
#   cron.present:
#   - name: rm /tmp/backintime.lock >/dev/null
#   - user: root
#   - minute: 30

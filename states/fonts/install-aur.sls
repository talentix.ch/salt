{% set user = grains.user                               %}
{% import_yaml slspath ~ "/fonts-aur.yaml" as fonts_aur %}

{% for font, font_specs in fonts_aur.fonts.items()      %}
{% set test = font_specs.test                           %}
{{ tplfile }}> Clone {{ font }}:
  git.cloned:
  - name: https://aur.archlinux.org/{{ font }}
  - user: {{ user }}
  - target: /home/{{ user }}/.local/sources/{{ font }}
  - creates: /home/{{ user }}/.local/sources/{{ font }}
 
{{ tplfile }}> Make and install package {{ font }}:
  cmd.run:
  - name: makepkg -sicC --noconfirm
  - runas: {{ user }}
  - cwd: /home/{{ user }}/.local/sources/{{ font }}
  - require:
    - {{ tplfile }}> Clone {{ font }}
  - onlyif: test -d /home/{{ user }}/.local/sources/{{ font }}
  - unless: test {{ test }}
{% endfor %}


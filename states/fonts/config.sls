# https://wiki.archlinux.org/title/Font_configuration

{{ tplfile }}> Make available preset fonts 10-nerd-font-symbols.conf:
  file.symlink:
  - name: /etc/fonts/conf.d/10-nerd-font-symbols.conf
  - target: /usr/share/fontconfig/conf.avail/10-nerd-font-symbols.conf
  - force: True

#?? {{ tplfile }}> Make available preset fonts 70-no-bitmaps.conf:
#??   file.symlink:
#??     - name: /etc/fonts/conf.d/70-no-bitmaps.conf
#??     - target: /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf

{{ tplfile }}> Make available preset fonts 10-scale-bitmap-fonts.conf:
  file.symlink:
  - name: /etc/fonts/conf.d/10-scale-bitmap-fonts.conf
  - target: /usr/share/fontconfig/conf.avail/10-scale-bitmap-fonts.conf
  - force: True

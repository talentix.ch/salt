{{ tplfile }}> Install font packages:
  pkg.installed:
  - pkgs:
    - gnu-free-fonts
    - gsfonts
    - ttf-opensans
    - ttf-ubuntu-font-family
    - ttf-iosevka-nerd
# ttf-nerd-fonts-symbols         ttf-nerd-fonts-symbols-common  ttf-nerd-fonts-symbols-mono
#      - adobe-source-code-pro-fonts
#       - noto-fonts
#       - terminus-font
#       - opendesktop-fonts
#       - ttf-bitstream-vera
#       - ttf-croscore
#       - ttf-dejavu
#       - ttf-droid
#       - ttf-font-awesome
#       - ttf-ibm-plex
#       - ttf-inconsolata
#       - ttf-joypixels
#       - ttf-liberation
#       - ttf-linux-libertine
#       - ttf-roboto
#       - ttf-roboto-mono


{% for firmware in ['ast-firmware', 'aic94xx-firmware', 'wd719x-firmware'] %}
{{ tplfile }}> Clone {{ firmware }}:
  git.cloned:
  - name: https://aur.archlinux.org/{{ firmware }}
  - user: {{ user }}
  - target: /home/{{ user }}/.local/sources/{{ firmware }}
  - creates: /home/{{ user }}/.local/sources/{{ firmware }}

{{ tplfile }}> Make and install package {{ firmware }}:
  cmd.run:
  - name: makepkg -sicC --noconfirm
  - runas: {{ user }}
  - cwd: /home/{{ user }}/.local/sources/{{ firmware }}
  - require:
    - {{ tplfile }}> Clone {{ firmware }}
  - onchanges: 
    - {{ tplfile }}> Clone {{ firmware }}
{% endfor %} 

{% set user = grains.user %}

{{ tplfile }}> Installing packages for {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}
  - pkgs:
    - zsh
    - zsh-completions

{{ tplfile }}> Change default shell to {{ slspath }} for user root:
  user.present:
  - name: root
  - shell: /bin/zsh
  - require:
    - {{ tplfile }}> Installing packages for {{ slspath }}

{{ tplfile }}> Restore .zsh_history for user {{ user }}:
  cmd.run:
  - name: cp -p /backup/restore-user/.zsh_history /home/{{ user }}/
  - runas: {{ user }}
  - unless: test -f /home/{{ user }}/.zsh_history
  - onlyif: test -d /home/{{ user }} && test -f /backup/restore-user/.zsh_history
  - failhard: True

{{ tplfile }}> Restore .zsh_all for user {{ user }}:
  cmd.run:
  - name: cp -p /backup/restore-user/.zsh_all /home/{{ user }}/
  - runas: {{ user }}
  - unless: test -f /home/{{ user }}/.zsh_all
  - onlyif: test -d /home/{{ user }} && test -f /backup/restore-user/.zsh_all
  - failhard: True

{{ tplfile }}> Restore .zsh_history for user root:
  cmd.run:
  - name: cp -p /backup/restore-root/.zsh_history /root/
  - unless: test -f /root/.zsh_history
  - onlyif: test -f /backup/restore-root/.zsh_history
  - require:
    - {{ tplfile }}> Installing packages for {{ slspath }}
  - failhard: True

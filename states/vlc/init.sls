{% set user = grains.user %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

# mkdir vlc/files/home/user/config
# cp -r ~/.config/vlc vlc/files/home/user/config/
# sed -i '/list-file/d' vlc/files/home/user/config/vlc/vlc-qt-interface.conf
# {{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
#   file.recurse:
#     - name: /home/{{ user }}/.config/{{ slspath }}
#     - source: salt://{{ slspath }}/files/home/user/config/{{ slspath }}
#     - makedirs: True
#     - user: {{ user }}
#     - group: {{ user }}
#     - require:
#       - {{ tplfile }}> Installing {{ slspath }}
#       - user: {{ user }}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

{{ tplfile }}> Enable {{ slspath }} and restart if config has changed:
  service.running:
  - name: {{ slspath }}
  - enable: True
#  - watch:
#     - file: /etc/cron.d/*
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{% set user     = grains.user %}
{% set fullname = 'Uri Liebeskind' if user == 'luri' else 'Uri' %}
{% set password = '$6$RfFQmmCvzBLkU8jm$7aK2ExcJ1TQobND2Tr9F7vZ.aJ/pKxAnMMVCHiLVdRaBcomq5N96lSOCAiVPwS9nRMKBWDhXx2WHlHdoRQQrB1' if user == 'uri' else '$6$1b5y/IIIyQP53NgF$k/XTseOFm8X7wQ0fXOwzpDgtFmXIM/7Y8aL1ixbNfO/y00B3JicvnD1rpUMSNyfTpSRDpac.S6QpP2evj57HJ.' %}

# The users to manage on the system. The password hashes can be generated with:
# python3 -c "from getpass import getpass; from crypt import *; p=getpass();print('\n'+crypt(p, METHOD_SHA512)) if p==getpass('Please repeat: ') else print('\nFailed repeating.')"
{{ tplfile }}> Create user {{ user }} and add group memberships:
  user.present:
  - name: {{ user }}
  - fullname: {{ fullname }}
  # - password: {{ password }}
  - home: /home/{{ user }}
  - usergroup: True
  - shell: /bin/zsh
  - groups:
    - wheel
    - storage
    - disk
    - games
    - power
    - cups
    - users
    - lp
    - kvm
    - qemu
    - libvirt
    - audio
    - plugdev
    - docker
#     - require:
#       - pkg: zsh

# {{ tplfile }}> Setting ownership for home of user {{ user }}:
#   file.directory:
#   - name: /home/{{ user }}
#   - user: {{ user }}
#   - group: {{ user }}

# {{ tplfile }}> Create .local directory for {{ user }}:
#   file.directory:
#   - name: /home/{{ user }}/.local
#   - user: {{ user }}
#   - group: {{ user }}
#   - dir_mode: '0755'
#   - makedirs: True
#   - recurse:
#     - user
#     - group

{% set user = grains.user                                            %}
{% set id   = 'qemu' if grains.manufacturer == 'QEMU' else grains.id %}

{{ tplfile }}> Installing packages for {{ slspath }}:
  pkg.installed:
  - pkgs:
    - i3-wm
    - i3lock
    - i3status
    - i3blocks

{{ tplfile }}> Install config {{ slspath }} files for user {{ user }}:
  file.recurse:
  - name: /home/{{ user }}/.config/{{ slspath }}
  - source: salt://{{ slspath }}/files/home/user/config/{{ slspath }}
  - exclude_pat: config.*
  - makedirs: True
  - user: {{ user }}
  - group: {{ user }}
  - require:
    - {{ tplfile }}> Installing packages for {{ slspath }}
    - user: {{ user }}

{{ tplfile }}> Install config {{ slspath }} files for user root:
  file.recurse:
  - name: /root/.config/{{ slspath }}
  - source: salt://{{ slspath }}/files/home/user/config/{{ slspath }}
  - exclude_pat: config.*
  - makedirs: True
  - require:
    - {{ tplfile }}> Installing packages for {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  file.managed:
  - name: /home/{{ user }}/.config/{{ slspath }}/config
  - source: salt://{{ slspath }}/files/home/user/config/{{ slspath }}/config.{{ id }}
  - makedirs: True
  - user: {{ user }}
  - group: {{ user }}
  - require:
    - {{ tplfile }}> Installing packages for {{ slspath }}
    - user: {{ user }}

{{ tplfile }}> Configure {{ slspath }} for user root:
  file.managed:
  - name: /root/.config/{{ slspath }}/config
  - source: salt://{{ slspath }}/files/home/user/config/{{ slspath }}/config.{{ id }}
  - makedirs: True
  - require:
    - {{ tplfile }}> Installing packages for {{ slspath }}

{{ tplfile }}> Copy saltstack master config files:
  file.recurse:
  - name: /etc/salt
  - source: salt://{{ slspath }}/files/etc/salt
  - makedirs: True

# {{ tplfile }}> Enable saltstack minion service:
#   service.running:
#   - name: salt-master
#   - enable: True
#   - watch:
#     - file: {{ tplfile }}> Copy saltstack master config files

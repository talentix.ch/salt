{% set restore_path_user = grains.restore_path_user %}
{% set restore_path_root = grains.restore_path_root %}

{{ tplfile }}> Check if grain variables to restore paths to user data exists:
  grains.exists:
  - names:
    - restore_path_user
    - restore_path_root
  - failhard: True

{{ tplfile }}> Check if directories exists:
  file.exists:
  - names:
    - {{ restore_path_user }}
    - {{ restore_path_root }}
  - failhard: True

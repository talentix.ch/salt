{% if grains.manufacturer == 'QEMU' %}
# {# {% set backup_device = '/dev/vdb' %} #} 
# {# # salt-call disk.blkid /dev/sda1 --out json | jq '.local."/dev/sda1".UUID' #}
# {# {% set uuid = salt.disk.blkid(backup_device)[backup_device]['UUID'] %}#}

{{ tplfile }}> Umount backup volume:
  mount.unmounted:
  - name: /backup
#{#    - device: UUID={{ uuid }} #}
#    - persist: True
  - failhard: True
{% endif %}

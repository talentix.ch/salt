{% set user = grains.user %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

# {{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
#   file.recurse:
#     - name: /home/{{ user }}/.config/{{ slspath }}rc
#     - source: salt://{{ slspath }}/files/home/user/config/{{ slspath }}rc
#     - makedirs: True
#     - user: {{ user }}
#     - group: {{ user }}
#     - require:
#       - {{ tplfile }}> Installing {{ slspath }}
#       - user: {{ user }}

#+ {{ tplfile }}> Configuing {{ slspath }} for user {{ user }}:
#+   cmd.run:
#+     - name: cp /backup/restore-user/.config/dolphinrc /home/{{ user }}/.config/
#+     - unless: test -f /home/{{ user }}/.config/dolphinrc
#+     - require:
#+       - {{ tplfile }}> Installing {{ slspath }}
##+    - failhard: True

#+ {{ tplfile }}> Restore {{ slspath }} data for user {{ user }}:
#+   cmd.run:
#+     - name: rsync -a /backup/restore-user/.local /home/{{ user }}/
#+     - onlyif: test -d /backup/restore-user/.local/share/{{ slspath }}
#+     - unless: test -d /home/{{ user }}/.local/share/{{ slspath }}
#+     - require:
#+       - {{ tplfile }}> Installing {{ slspath }}
#+    - failhard: True

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

{{ tplfile }}> Enable and start {{ slspath }} service:
  service.running:
  - name: {{ slspath }}
  - enable: True
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

# {{ tplfile }}> Enable and start cups-browsed.service service:
#   service.running:
#   - name: cups-browsed.service
#   - enable: True
#   - require:
#     - {{ tplfile }}> Installing {{ slspath }}

#!/usr/bin/env bash
#

# set -uo pipefail
# trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

#===============================================================================
err_exit() {
  echo
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" | grep -P --color ".+"
  echo "!!! ERROR: $1" | grep -P --color ".+"
  echo "!!!        Aborting script!" | grep -P --color ".+"
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" | grep -P --color ".+"
  echo
  exit 1
}


#===============================================================================
check_backup_path() {
  echo
  echo "=== $FUNCNAME"

  test -d /backup/restore-user || err_exit "Backup path not found: /backup/restore-user. Aborting!"
  test -d /backup/restore-etc  || err_exit "Backup path not found: /backup/restore-etc. Aborting!"

  echo "    +++ Backup paths found: /backup/restore-user, /backup/restore-root, /backup/restore-etc."

  return 0
}


#===============================================================================
create_ssh_config() {
  echo
  echo "=== $FUNCNAME"

  # SSH Config
  echo "    +++ Restoring ssh config"
  cp /backup/restore-etc/ssh/sshd_config /etc/ssh/
  systemctl restart sshd.service

  echo "    +++ Creating /root/.ssh"
  test -d /root/.ssh || mkdir -m 700 /root/.ssh || err_exit "Failed to create /root/.ssh!"

  # Copy ssh config from backup
  echo "    +++ Copying ssh keys from backup to /root/.ssh"
  if [[ -d /backup/restore-root/.ssh ]] ; then 
    cp -r /backup/restore-root/.ssh /root/ || err_exit "Failed to copy/create /root/.ssh from root!"
  else
    cp -r /backup/restore-user/.ssh /root/ || err_exit "Failed to copy/create /root/.ssh from user!"
  fi  

  # Delete uneccessary files
  find /root/.ssh/ -type f -not -name "id_ed25519*" -not -name config -not -name authorized_keys -not -name known_hosts -delete
  echo "    +++ Showing .ssh folder:"
  ls -ald /root/.ssh/*
  read -p "    >>> Press ENTER to continue or Ctrl+C to abort."
}


#===============================================================================
configure_saltstack() {
  echo
  echo "=== $FUNCNAME"

  echo "${hostname}" > /etc/salt/minion_id

  # Save stdout
  # exec 3>&1

  # Redirect all stdout to config file /etc/salt/minion
  # exec 1> /etc/salt/minion

  # Restore stdout
  # exec 1>&3

  echo "    +++ Creating saltstack minion config path: /etc/salt/minion.d"
  test -d /etc/salt/minion.d || mkdir /etc/salt/minion.d || err_exit "Failed to create /etc/salt/minion.d!"

  echo "    +++ Creating config file /etc/salt/minion.d/file-directory.conf"
  cat >/etc/salt/minion.d/file-directory.conf <<EOF
file_client: local


# Saltstack is configured to run in masterless mode.
# This requires some configs of the salt-master (/etc/salt/master)
# to be added to the saltstack client (/etc/salt/minion).

top_file_merging_strategy: same

file_roots:
  base:
    - /srv/salt/states

pillar_roots:
  base:
    - /srv/salt/pillars
EOF
  read -p ">>> Showing config file. Press ENTER to continue."
  cat /etc/salt/minion.d/file-directory.conf
  read -p ">>> Press ENTER to continue."

  echo "    +++ Restart saltstack minion service"
  systemctl restart salt-minion.service
 
  echo "    +++ Define user"
  user=$(basename $(readlink /backup/restore-user))
  salt-call grains.set user $user
}


#===============================================================================
prepare_saltstack() {
  eco
  echo "=== $FUNCNAME"

  cd /srv/
  rm -rf /srv/salt
  git clone git@gitlab.com:talentix.ch/salt.git salt

  return 0
}

#===============================================================================
# MAIN
#===============================================================================
# https://gitlab.com/talentix.ch/salt/-/blob/main/install.sh
# https://gitlab.com/talentix.ch/salt/-/blob/main/install.sh
# https://gitlab.com/talentix.ch/salt/-/raw/main/install.sh


check_backup_path
create_ssh_config
configure_saltstack
prepare_saltstack


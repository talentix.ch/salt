{% set user = grains.user %}

# Source: https://rasfw-lb-01-v1001.zhaw.ch/CACHE/stc/3/binaries/anyconnect-linux64-4.10.05111-core-vpn-webdeploy-k9.sh
# Prerequisites: openvpn and openconnect

{{ tplfile }}> Install prerequisite packages packages:
  pkg.installed:
  - pkgs:
    - gtk3
    - glib2
    - webkit2gtk
#    - gtk2

{{ tplfile }}> Installing {{ slspath }}:
  cmd.run:
  - name: bash anyconnect-linux64-4.10.05111-core-vpn-webdeploy-k9.sh 
  - cwd: /backup/data/Software/Cisco
  - unless: test -f /opt/cisco/anyconnect/bin/vpnui
  - require:
    - {{ tplfile }}> Install prerequisite packages packages

{{ tplfile }}> Restore {{ slspath }} profile for zhaw:
  cmd.run:
  - name: cp -p /backup/restore-opt/cisco/anyconnect/profile/ZHAW.xml /opt/cisco/anyconnect/profile/
  - unless: test -f /opt/cisco/anyconnect/profile/ZHAW.xml
  - onlyif: test -f /backup/restore-opt/cisco/anyconnect/profile/ZHAW.xml 
  - require:
    - {{ tplfile }}> Installing {{ slspath }}
    - user: {{ user }}
  - failhard: True

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  cmd.run:
  - name: cp -p /backup/restore-user/.anyconnect /home/{{ user }}/
  - runas: {{ user }}
  - unless: grep -iq zhaw.ch /home/{{ user }}/.anyconnect
  - onlyif: test -f /backup/restore-user/.anyconnect 
  - require:
    - {{ tplfile }}> Installing {{ slspath }}
    - user: {{ user }}
  - failhard: True

{{ tplfile }}> Create symlink to anyconnect vpnui:
  file.symlink:
  - name: /usr/local/bin/vpnui
  - target: /opt/cisco/anyconnect/bin/vpnui
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

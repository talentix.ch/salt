{{ tplfile }}> Add locale de_CH.UTF-8:
  locale.present:
  - names: 
    - de_CH.UTF-8 UTF-8
    - en_DK.UTF-8 UTF-8
    - en_US.UTF-8 UTF-8

#{{ tplfile }}> Run locale-gen if /etc/locale.gen has changed:
#  cmd.run:
#  - name: "locale-gen"
#  - unless: diff <(md5sum /etc/locale.gen) <(cat /etc/locale.gen.md5)
#
#{{ tplfile }}> Check if /etc/locale.gen has changed:
#  cmd.run:
#  - name: "md5sum /etc/locale.gen > /etc/locale.gen.md5"
#  - require:
#     - {{ tplfile }}> Run locale-gen if /etc/locale.gen has changed
#  - onchanges:
#    - {{ tplfile }}> Run locale-gen if /etc/locale.gen has changed

{{ tplfile }}> Set default locale /etc/locale.conf:
  file.managed:
  - name: /etc/locale.conf
  - contents: |
      LANG="en_US.UTF-8"
      LC_NUMERIC="de_CH.UTF-8"
      LC_TIME="en_DK.UTF-8"
      LC_MONETARY="de_CH.UTF-8"
      LC_PAPER="de_CH.UTF-8"
      LC_ADDRESS="de_CH.UTF-8"
      LC_TELEPHONE="de_CH.UTF-8"
      LC_MEASUREMENT="de_CH.UTF-8"
#      TIME_STYLE="posix-long-iso"
#  - bufsize: file


# LOCALIZATION

# Default / system locales are stored in:
# Archlinux: /etc/locale.conf
# Ubuntu:    /etc/default/locale

# User defined locales are stored in: ~/.config/locale.conf
# /etc/profile.d/locale.sh

# Show current locales in use on system
# locale
# locale: Cannot set LC_CTYPE to default locale: No such file or directory
# locale: Cannot set LC_MESSAGES to default locale: No such file or directory
# locale: Cannot set LC_ALL to default locale: No such file or directory
# LANG=en_US.UTF-8
# LC_CTYPE="en_US.UTF-8"
# LC_NUMERIC="en_US.UTF-8"
# LC_TIME="en_US.UTF-8"
# LC_COLLATE="en_US.UTF-8"
# LC_MONETARY="en_US.UTF-8"
# LC_MESSAGES=
# LC_PAPER="en_US.UTF-8"
# LC_NAME="en_US.UTF-8"
# LC_ADDRESS="en_US.UTF-8"
# LC_TELEPHONE="en_US.UTF-8"
# LC_MEASUREMENT="en_US.UTF-8"
# LC_IDENTIFICATION="en_US.UTF-8"
# LC_ALL=

# Show all (installed/available) locales
# locale -a

#{{ tplfile }}> Set us inmternational keyboard:
#  keyboard.system:
#  - name: us

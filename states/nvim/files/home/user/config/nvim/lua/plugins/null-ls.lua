return {
  {
    "jose-elias-alvarez/null-ls.nvim",
    event = { "BufReadPre", "BufNewFile" },
    dependencies = { "mason.nvim" },
    opts = function(_, opts)
      local nls = require("null-ls")
      table.insert(opts.sources, nls.builtins.formatting.shfmt.with({ extra_args = { "-i", 2 } }))
      table.insert(opts.sources, nls.builtins.formatting.black) -- Python autoformatter. Brauchst Du warscheinlich nicht
      table.insert(opts.sources, nls.builtins.formatting.isort) -- Python imports sorter. Bracuhst Du warscheinlich auch nicht.
      return opts
    end,
  },
}

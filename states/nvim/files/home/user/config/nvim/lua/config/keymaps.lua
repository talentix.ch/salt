-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

-- Modes
-- normal_mode = "n",
-- insert_mode = "i",
-- visual_mode = "v",
-- visual_block_mode = "x",
-- term_mode = "t",
-- command_mode = "c",

Util = require("lazyvim.util")

local function map(mode, lhs, rhs, opts)
  local keys = require("lazy.core.handler").handlers.keys
  ---@cast keys LazyKeysHandler
  -- do not create the keymap if a lazy keys handler exists
  if not keys.active[keys.parse({ lhs, mode = mode }).id] then
    opts = opts or {}
    opts.silent = opts.silent ~= false
    vim.keymap.set(mode, lhs, rhs, opts)
  end
end

-- Switch from insert mode to normal mode
map("i", "jj", "<esc>", { desc = "Faster ESC" })

-- Swap : and ;
-- map("n", ":", ";", { desc = "Let : be ;" })
-- map("n", ";", ":", { desc = "Let ; be :" })
vim.cmd([[nnoremap ; :]])
vim.cmd([[nnoremap : ;]])

-- Better up/down in wraped lines
map("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
map("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })

-- Let n always search down and N always search up, regardless of searching with / or ?
-- https://github.com/mhinz/vim-galore#saner-behavior-of-n-and-n
map("n", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
map("x", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
map("o", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
map("n", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })
map("x", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })
map("o", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })

-- Stay in indent mode
map("v", "<", "<gv", { desc = "Indent left" })
map("v", ">", ">gv", { desc = "Indent right" })

-- Turn off highlight after search
map({ "i", "n" }, "<esc>", "<cmd>noh<cr><esc>", { desc = "Escape and clear hlsearch" })

-- Jump to 1-st char
map("n", "<c-0>", "^", { desc = "Jump to 1-st char in line" })

-- Quick select to eol and paste
map("n", "vp", "v$p", { desc = "Quick select and paste" })

-- Source buffer
map("n", "<c-s>", "<cmd>so %<cr>", { desc = "Source buffer" })

-- Additional mappings for pgdn and pgup
map("n", "<c-a-j>", "<c-f>")
map("n", "<c-a-k>", "<c-b>")

-- Scroll up/down while keeping the cursor on the current position
map("n", "<a-j>", "<c-e>", { desc = "Scroll up" })
map("n", "<a-k>", "<c-y>", { desc = "Scroll down" })

-- Move lines
map("n", "<s-a-j>", "<cmd>m .+1<cr>==", { desc = "Move down" })
map("n", "<s-a-k>", "<cmd>m .-2<cr>==", { desc = "Move up" })
map("i", "<s-a-j>", "<esc><cmd>m .+1<cr>==gi", { desc = "Move down" })
map("i", "<s-a-k>", "<esc><cmd>m .-2<cr>==gi", { desc = "Move up" })
map("v", "<s-a-j>", ":m '>+1<cr>gv=gv", { desc = "Move down" })
map("v", "<s-a-k>", ":m '<-2<cr>gv=gv", { desc = "Move up" })

-- Go to split window using <c-{h,j,k,l,p}>
map("n", "<c-h>", "<c-w>h", { desc = "Go to left window", remap = true })
map("n", "<c-j>", "<c-w>j", { desc = "Go to lower window", remap = true })
map("n", "<c-k>", "<c-w>k", { desc = "Go to upper window", remap = true })
map("n", "<c-l>", "<c-w>l", { desc = "Go to right window", remap = true })
map("n", "<c-p>", "<c-w>p", { desc = "Last (previous) window", remap = true })

-- Create or close split window using <c-{|,-,x}>
map("n", "<space>-", "<c-w>s", { desc = "Split window below", remap = true })
map("n", "<space>|", "<c-w>v", { desc = "Split window right", remap = true })
map("n", "<space>/", "<c-w>v", { desc = "Split window right", remap = true })
map("n", "<c-x>", "<c-w>c", { desc = "Close window", remap = true })

-- Save / close buffer/s
map({ "i", "v", "n", "s" }, "<a-s>", "<cmd>w<cr><esc>", { desc = "Save buffer" })
map({ "i", "v", "n", "s" }, "<s-a-s>", "<cmd>wa<cr><esc>", { desc = "Save all buffers" })
map({ "i", "v", "n", "s" }, "<a-x>", "<cmd>bp|bd! #<cr>", { desc = "Close buffer" })
map({ "i", "v", "n", "s" }, "<s-a-x>", "<cmd>q!<cr>", { desc = "Close all buffers" })

-- Go to buffer or tab
-- Go to tab
-- map("n", "<S-h>", "<cmd>BufferLineCyclePrev<cr>", { desc = "Prev buffer" })
-- map("n", "<S-l>", "<cmd>BufferLineCycleNext<cr>", { desc = "Next buffer" })
map("n", "<a-l>", "<cmd>BufferNext<cr>", { desc = "Go to next tab", remap = true })
map("n", "<a-h>", "<cmd>BufferPrevious<cr>", { desc = "Go to previous tab", remap = true })
map("n", "<a-right>", "<cmd>BufferNext<cr>", { desc = "Go to next tab", remap = true })
map("n", "<a-left>", "<cmd>BufferPrevious<cr>", { desc = "Go to previous butab", remap = true })
-- Moving tabs
map("n", "<s-a-l>", "<cmd>BufferMoveNext<cr>", { desc = "Move tab right", remap = true })
map("n", "<s-a-h>", "<cmd>BufferMovePrev<cr>", { desc = "Move tab left", remap = true })
map("n", "<s-a-right>", "<cmd>BufferMoveNext<cr>", { desc = "Move tab right", remap = true })
map("n", "<s-a-left>", "<cmd>BufferMovePrev<cr>", { desc = "Move tab left", remap = true })
map("n", "<s-a-0>", "<cmd>BufferMoveStart<cr>", { desc = "Move tab start", remap = true })

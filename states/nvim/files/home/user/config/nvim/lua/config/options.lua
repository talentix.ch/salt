-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

local opt = vim.opt

opt.scrolloff = 2 -- Lines of context before scrolling up/down
opt.sidescrolloff = 2 -- Lines of context before scrolling right/left
opt.expandtab = true -- convert tabs to spaces
opt.selection = "exclusive"
-- opt.norestartofline = true

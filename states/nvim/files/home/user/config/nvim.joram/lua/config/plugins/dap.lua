return {
  {
    "mfussenegger/nvim-dap",

    dependencies = {

      -- fancy UI for the debugger
      {
        "rcarriga/nvim-dap-ui",
        -- stylua: ignore
        keys = {
          { "<leader>du", function() require("dapui").toggle({ }) end, desc = "Dap UI" },
          { "<leader>de", function() require("dapui").eval() end, desc = "Eval", mode = {"n", "v"} },
        },
        opts = {},
        config = function(_, opts)
          local dap = require("dap")
          local dapui = require("dapui")
          dapui.setup(opts)
          dap.listeners.after.event_initialized["dapui_config"] = function()
            dapui.open({})
          end
          dap.listeners.before.event_terminated["dapui_config"] = function()
            dapui.close({})
          end
          dap.listeners.before.event_exited["dapui_config"] = function()
            dapui.close({})
          end
        end,
      },

      -- virtual text for the debugger
      {
        "theHamsta/nvim-dap-virtual-text",
        opts = {},
      },

      -- which key integration
      {
        "folke/which-key.nvim",
        optional = true,
        opts = {
          defaults = {
            ["<leader>d"] = { name = "+debug" },
            ["<leader>da"] = { name = "+adapters" },
          },
        },
      },

      -- mason.nvim integration
      {
        "jay-babu/mason-nvim-dap.nvim",
        dependencies = "mason.nvim",
        cmd = { "DapInstall", "DapUninstall" },
        opts = {
          -- Makes a best effort to setup the various debuggers with
          -- reasonable debug configurations
          automatic_installation = true,

          -- You can provide additional configuration to the handlers,
          -- see mason-nvim-dap README for more information
          handlers = {},

          -- You'll need to check that you have the required things installed
          -- online, please don't ask me how to install them :)
          ensure_installed = {
            "debugpy",
          },
        },
      },
      {
        "jbyuki/one-small-step-for-vimkind",
        -- stylua: ignore
        keys = {
          { "<leader>daL", function() require("osv").launch({ port = 8086 }) end, desc = "Adapter Lua Server" },
          { "<leader>dal", function() require("osv").run_this() end, desc = "Adapter Lua" },
        },
        config = function()
          local dap = require("dap")
          dap.adapters.nlua = function(callback, config)
            callback({ type = "server", host = config.host or "127.0.0.1", port = config.port or 8086 })
          end
          dap.configurations.lua = {
            {
              type = "nlua",
              request = "attach",
              name = "Attach to running Neovim instance",
            },
          }
        end,
      },
    },

    -- stylua: ignore
    keys = {
      { "<leader>dB", function() require("dap").set_breakpoint(vim.fn.input('Breakpoint condition: ')) end, desc = "Breakpoint Condition" },
      { "<leader>db", function() require("dap").toggle_breakpoint() end, desc = "Toggle Breakpoint" },
      { "<leader>dc", function() require("dap").continue() end, desc = "Continue" },
      { "<leader>dC", function() require("dap").run_to_cursor() end, desc = "Run to Cursor" },
      { "<leader>dg", function() require("dap").goto_() end, desc = "Go to line (no execute)" },
      { "<leader>di", function() require("dap").step_into() end, desc = "Step Into" },
      { "<leader>dj", function() require("dap").down() end, desc = "Down" },
      { "<leader>dk", function() require("dap").up() end, desc = "Up" },
      { "<leader>dl", function() require("dap").run_last() end, desc = "Run Last" },
      { "<leader>do", function() require("dap").step_out() end, desc = "Step Out" },
      { "<leader>dO", function() require("dap").step_over() end, desc = "Step Over" },
      { "<leader>dp", function() require("dap").pause() end, desc = "Pause" },
      { "<leader>dr", function() require("dap").repl.toggle() end, desc = "Toggle REPL" },
      { "<leader>ds", function() require("dap").session() end, desc = "Session" },
      { "<leader>dt", function() require("dap").terminate() end, desc = "Terminate" },
      { "<leader>dw", function() require("dap.ui.widgets").hover() end, desc = "Widgets" },
    },

    config = function()
      local Config = require("config.util")
      vim.api.nvim_set_hl(0, "DapStoppedLine", { default = true, link = "Visual" })

      local launch_json = nil
      local root = require("config.util").get_root()
      vim.notify(root)
      if vim.loop.fs_stat(root .. "/lauanch.json") then
        launch_json = root .. "/launch.json"
      elseif vim.loop.fs_stat(root .. "/.vscode/launch.json") then
        launch_json = root .. "./.vscode/launch.json"
      end

      vim.notify("Setting up fs event something something", vim.log.levels.INFO)

      local fs_event_file_in_root = vim.loop.new_fs_event()
      local fs_event_file_in_vscode = vim.loop.new_fs_event()
      assert(fs_event_file_in_root ~= nil)
      assert(fs_event_file_in_vscode ~= nil)

      local function on_launchjson_change(err, filename, events)
        if err ~= nil then
          err = err .. " "
        else
          err = ""
        end
        print(
          err
            .. filename
            .. " events.change: "
            .. tostring(events.change)
            .. ", events.rename: "
            .. tostring(events.rename)
        )
      end

      vim.loop.run("default")

      local launch_json_root = root .. "/launch.json"
      fs_event_file_in_root:start(
        launch_json_root,
        { watch_entry = true, stat = true, recursive = true },
        vim.schedule_wrap(on_launchjson_change)
      )

      -- fs_event_file_in_root:start(root .. "/launch.json", { watch_entry = true, stat = true }, on_launchjson_change)
      -- fs_event_file_in_vscode:start(
      --   root .. "/.vscode/launch.json",
      --   { watch_entry = true, stat = true },
      --   on_launchjson_change
      -- )

      for name, sign in pairs(Config.icons.dap) do
        sign = type(sign) == "table" and sign or { sign }
        vim.fn.sign_define(
          "Dap" .. name,
          { text = sign[1], texthl = sign[2] or "DiagnosticInfo", linehl = sign[3], numhl = sign[3] }
        )
      end
    end,
  },
  {
    "folke/neoconf.nvim",
    cmd = "Neoconf",
    config = function(_, opts)
      local dap = require("dap")
      require("neoconf.plugins").register({
        on_schema = function(schema)
          schema:import("dap.configurations", dap.configurations)
        end,
      })
    end,
  },
}

return {
  "EdenEast/nightfox.nvim",
  lazy = false,
  priority = 1000,
  opts = {
    colorscheme = "nightfox",
  },
  config = function(_, opts)
    require("nightfox").setup(opts)
    vim.cmd.colorscheme("nightfox")
  end,
}

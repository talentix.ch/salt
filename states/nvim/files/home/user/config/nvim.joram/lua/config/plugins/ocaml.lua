return {
  "neovim/nvim-lspconfig",
  event = { "BufReadPre", "BufNewFile" },
  servers = {
    ocamllsp = {
      cmd = { "/home/joram/.opam/ocaml-variants.4.11.1+flambda/bin/ocamllsp" },
    },
  },
}

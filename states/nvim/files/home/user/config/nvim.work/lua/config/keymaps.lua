-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

-- Modes
-- normal_mode = "n",
-- insert_mode = "i",
-- visual_mode = "v",
-- visual_block_mode = "x",
-- term_mode = "t",
-- command_mode = "c",

local Util = require("lazyvim.util")

local function map(mode, lhs, rhs, opts)
  local keys = require("lazy.core.handler").handlers.keys
  ---@cast keys LazyKeysHandler
  -- do not create the keymap if a lazy keys handler exists
  if not keys.active[keys.parse({ lhs, mode = mode }).id] then
    opts = opts or {}
    opts.silent = opts.silent ~= false
    vim.keymap.set(mode, lhs, rhs, opts)
  end
end

-- Swap : and ;
-- map("n", ":", ";", { desc = "Let : be ;" })
-- map("n", ";", ":", { desc = "Let ; be :" })
vim.cmd([[nnoremap ; :]])
vim.cmd([[nnoremap : ;]])

-- Jump to 1-st char
map("n", "<c-0>", "^", { desc = "Jump to 1-st char in line" })

-- Quick select to eol and paste
map("n", "vp", "v$p", { desc = "Quick select and paste" })

-- Switch from insert mode to normal mode
map("i", "jj", "<esc>", { desc = "Faster ESC" })
map("i", "kk", "<esc>", { desc = "Faster ESC" })
map("i", "jk", "<esc>", { desc = "Faster ESC" })
map("i", "kj", "<esc>", { desc = "Faster ESC" })

-- Window navigation
map("n", "<c-x>", "<c-w>c") -- Close window

-- Buffer navigation
map("n", "<a-l>", "<cmd>BufferNext<cr>") -- Go to the tab right (alt-l)
map("n", "<a-h>", "<cmd>BufferPrevious<cr>") -- Go to the tab left (alt-h)
map("n", "<a-right>", "<cmd>BufferNext<cr>") -- Go to the tab right (alt-right)
map("n", "<a-left>", "<cmd>BufferPrevious<cr>") -- Go to the tab left (alt-left)
map("n", "<a-x>", "<cmd>bp|bd#<cr>") -- Close buffer
map("n", "<s-a-x>", "<cmd>qa<cr>") -- Quit all buffers

-- Moving buffers
map("n", "<s-a-h>", "<cmd>BufferMovePrev<cr>") -- Move tab left  (shift-alt-h)
map("n", "<s-a-l>", "<cmd>BufferMoveNext<cr>") -- Move tab right (shift-alt-l)
map("n", "<s-a-right>", "<cmd>BufferMoveNext<cr>") -- Move tab right
map("n", "<s-a-left>", "<cmd>BufferMovePrev<cr>") -- Move tab left
map("n", "<s-a-0>", "<cmd>BufferMoveStart<cr>") -- Move tab to the beginning

-- Save / Close buffer
map({ "i", "v", "n", "s" }, "<a-s>", "<cmd>w<cr><esc>", { desc = "Save buffer" })
map({ "i", "v", "n", "s" }, "<s-a-s>", "<cmd>wa<cr><esc>", { desc = "Save all buffers" })

-- Scroll up/down while keeping the cursor on the current position
map("n", "<c-down>", "<c-e>")
map("n", "<a-j>", "<c-e>")
map("n", "<c-up>", "<c-y>")
map("n", "<a-k>", "<c-y>")

-- Additional mappings for pgdn and pgup
map("n", "<c-a-j>", "<c-f>")
map("n", "<c-a-k>", "<c-b>")

-- Move Lines
map("n", "<s-a-j>", "<cmd>m .+1<cr>==", { desc = "Move down" })
map("n", "<s-a-k>", "<cmd>m .-2<cr>==", { desc = "Move up" })
map("i", "<s-a-j>", "<esc><cmd>m .+1<cr>==gi", { desc = "Move down" })
map("i", "<s-a-k>", "<esc><cmd>m .-2<cr>==gi", { desc = "Move up" })
map("v", "<s-a-j>", ":m '>+1<cr>gv=gv", { desc = "Move down" })
map("v", "<s-a-k>", ":m '<-2<cr>gv=gv", { desc = "Move up" })

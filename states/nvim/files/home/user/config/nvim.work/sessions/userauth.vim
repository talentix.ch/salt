let SessionLoad = 1
let s:so_save = &g:so | let s:siso_save = &g:siso | setg so=0 siso=0 | setl so=-1 siso=-1
let v:this_session=expand("<sfile>:p")
silent only
silent tabonly
cd ~/git/join-ad/states/master/common/ubuntu22only/userauth
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
let s:shortmess_save = &shortmess
if &shortmess =~ 'A'
  set shortmess=aoOA
else
  set shortmess=aoO
endif
badd +9 install.sls
badd +17 config.sls
badd +11 files/etc/sssd/conf.d/00-zhaw.conf
badd +32 README
badd +3 ~/git/join-ad/states/master/common/ubuntu22only/join-ad/config.sls
badd +3 ~/git/join-ad/states/master/common/ubuntu22only/init.sls
badd +9 ~/git/join-ad/states/master/common/ubuntu22only/sysupdates/initialupdate.sls
badd +19 ~/git/join-ad/states/master/common/ubuntu22only/sysupdates/install.sls
badd +6 ~/git/join-ad/states/master/common/ubuntu22only/sysupdates/files/etc/cron.d/zhaw-sysupdates
argglobal
%argdel
$argadd init.sls
$argadd install.sls
$argadd config.sls
$argadd service.sls
$argadd files/zhaw
$argadd files/system-auth-ac
$argadd ./files/scripts/create-ldap-groups.sh
$argadd files/oddjobd-mkhomedir.conf
$argadd files/common-session
$argadd files/etc/krb5.conf
$argadd files/etc/nsswitch.conf
$argadd files/etc/sssd/conf.d/00-zhaw.conf
$argadd files/etc/pam.d/common-account
$argadd files/etc/pam.d/common-auth
$argadd files/etc/pam.d/common-password
$argadd files/etc/pam.d/common-session
edit ~/git/join-ad/states/master/common/ubuntu22only/join-ad/config.sls
argglobal
if bufexists(fnamemodify("~/git/join-ad/states/master/common/ubuntu22only/join-ad/config.sls", ":p")) | buffer ~/git/join-ad/states/master/common/ubuntu22only/join-ad/config.sls | else | edit ~/git/join-ad/states/master/common/ubuntu22only/join-ad/config.sls | endif
if &buftype ==# 'terminal'
  silent file ~/git/join-ad/states/master/common/ubuntu22only/join-ad/config.sls
endif
balt config.sls
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let &fdl = &fdl
let s:l = 14 - ((13 * winheight(0) + 35) / 71)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 14
normal! 028|
tabnext 1
if exists('s:wipebuf') && len(win_findbuf(s:wipebuf)) == 0 && getbufvar(s:wipebuf, '&buftype') isnot# 'terminal'
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20
let &shortmess = s:shortmess_save
let s:sx = expand("<sfile>:p:r")."x.vim"
if filereadable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &g:so = s:so_save | let &g:siso = s:siso_save
set hlsearch
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :

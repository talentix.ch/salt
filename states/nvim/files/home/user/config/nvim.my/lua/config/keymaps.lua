local util = require("config.util")
local map = util.map

-- Modes
--   normal = "n",
--   insert = "i",
--   visual = "v",
--   visual_block = "x",
--   term = "t",
--   command = "c",

-- Remap space as leader key
-- keymap("", "<Space>", "<Nop>", opts)
-- vim.g.mapleader = " "
-- vim.g.maplocalleader = " "

-- Switch from insert mode to normal mode
map("i", "jj", "<esc>", { desc = "Faster ESC" })

-- Swap : and ;
-- map("n", ":", ";", { desc = "Let : be ;" })
-- map("n", ";", ":", { desc = "Let ; be :" })
vim.cmd([[nnoremap ; :]])
vim.cmd([[nnoremap : ;]])

-- Better up/down in wraped lines
map("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
map("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })

-- Let n always search down and N always search up, regardless of searching with / or ?
-- https://github.com/mhinz/vim-galore#saner-behavior-of-n-and-n
map("n", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
map("x", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
map("o", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
map("n", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })
map("x", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })
map("o", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })

-- Stay in indent mode
map("v", "<", "<gv", { desc = "Indent left" })
map("v", ">", ">gv", { desc = "Indent right" })

-- Turn off highlight after search
map({ "i", "n" }, "<esc>", "<cmd>noh<cr><esc>", { desc = "Escape and clear hlsearch" })

-- Jump to 1-st char
map("n", "<c-0>", "^", { desc = "Jump to 1-st char in line" })

-- Quick select to eol and paste
map("n", "vp", "v$p", { desc = "Quick select and paste" })

-- Source buffer
map("n", "<c-s>", "<cmd>so %<cr>", { desc = "Source buffer" })

-- Additional mappings for pgdn and pgup
map("n", "<c-a-j>", "<c-f>")
map("n", "<c-a-k>", "<c-b>")

-- Scroll up/down while keeping the cursor on the current position
map("n", "<a-j>", "<c-e>", { desc = "Scroll up" })
map("n", "<a-k>", "<c-y>", { desc = "Scroll down" })

--+ -- Visual Block --
--+ -- Move text up and down
--+ keymap("x", "J", ":move '>+1<cr>gv-gv", opts)
--+ keymap("x", "K", ":move '<-2<cr>gv-gv", opts)
--+ keymap("x", "<a-j>", ":move '>+1<cr>gv-gv", opts)
--+ keymap("x", "<a-k>", ":move '<-2<cr>gv-gv", opts)

-- Move lines
map("n", "<s-a-j>", "<cmd>m .+1<cr>==", { desc = "Move down" })
map("n", "<s-a-k>", "<cmd>m .-2<cr>==", { desc = "Move up" })
map("i", "<s-a-j>", "<esc><cmd>m .+1<cr>==gi", { desc = "Move down" })
map("i", "<s-a-k>", "<esc><cmd>m .-2<cr>==gi", { desc = "Move up" })
map("v", "<s-a-j>", ":m '>+1<cr>gv=gv", { desc = "Move down" })
map("v", "<s-a-k>", ":m '<-2<cr>gv=gv", { desc = "Move up" })

-- Go to split window using <c-{h,j,k,l,p}>
map("n", "<c-h>", "<c-w>h", { desc = "Go to left window", remap = true })
map("n", "<c-j>", "<c-w>j", { desc = "Go to lower window", remap = true })
map("n", "<c-k>", "<c-w>k", { desc = "Go to upper window", remap = true })
map("n", "<c-l>", "<c-w>l", { desc = "Go to right window", remap = true })
map("n", "<c-p>", "<c-w>p", { desc = "Last (previous) window", remap = true })

-- Create or close split window using <c-{|,-,x}>
map("n", "<space>-", "<c-w>s", { desc = "Split window below", remap = true })
map("n", "<space>|", "<c-w>v", { desc = "Split window right", remap = true })
map("n", "<space>/", "<c-w>v", { desc = "Split window right", remap = true })
map("n", "<c-x>", "<c-w>c", { desc = "Close window", remap = true })

-- Save / close buffer/s
map({ "i", "v", "n", "s" }, "<a-s>", "<cmd>w<cr><esc>", { desc = "Save buffer" })
map({ "i", "v", "n", "s" }, "<s-a-s>", "<cmd>wa<cr><esc>", { desc = "Save all buffers" })
map({ "i", "v", "n", "s" }, "<a-x>", "<cmd>bp|bd!#<cr>", { desc = "Close buffer" })
map({ "i", "v", "n", "s" }, "<s-a-x>", "<cmd>q!<cr>", { desc = "Close all buffers" })

-- Go to buffer or tab
if util.has("barbar.nvim") then
  -- Go to tab
  -- map("n", "<S-h>", "<cmd>BufferLineCyclePrev<cr>", { desc = "Prev buffer" })
  -- map("n", "<S-l>", "<cmd>BufferLineCycleNext<cr>", { desc = "Next buffer" })
  map("n", "<a-l>", "<cmd>BufferNext<cr>", { desc = "Go to next tab", remap = true })
  map("n", "<a-h>", "<cmd>BufferPrevious<cr>", { desc = "Go to previous tab", remap = true })
  map("n", "<a-right>", "<cmd>BufferNext<cr>", { desc = "Go to next tab", remap = true })
  map("n", "<a-left>", "<cmd>BufferPrevious<cr>", { desc = "Go to previous butab", remap = true })
  -- Moving tabs
  map("n", "<s-a-l>", "<cmd>BufferMoveNext<cr>", { desc = "Move tab right", remap = true })
  map("n", "<s-a-h>", "<cmd>BufferMovePrev<cr>", { desc = "Move tab left", remap = true })
  map("n", "<s-a-right>", "<cmd>BufferMoveNext<cr>", { desc = "Move tab right", remap = true })
  map("n", "<s-a-left>", "<cmd>BufferMovePrev<cr>", { desc = "Move tab left", remap = true })
  map("n", "<s-a-0>", "<cmd>BufferMoveStart<cr>", { desc = "Move tab start", remap = true })
else
  -- Go to buffer
  map("n", "<a-l>", "<cmd>bnext<cr>", { desc = "Next buffer" })
  map("n", "<a-h>", "<cmd>bprevious<cr>", { desc = "Prev buffer" })
  map("n", "<a-right>", "<cmd>bnext<cr>", { desc = "Next buffer" })
  map("n", "<a-left>", "<cmd>bprevious<cr>", { desc = "Prev buffer" })
end

-- === To find out ===
-- === -- Clear search, diff update and redraw
-- === -- taken from runtime/lua/_editor.lua
-- === map(
-- ===   "n",
-- ===   "<leader>ur",
-- ===   "<Cmd>nohlsearch<Bar>diffupdate<Bar>normal! <C-L><CR>",
-- ===   { desc = "Redraw / clear hlsearch / diff update" }
-- === )
-- ===
-- === map({ "n", "x" }, "gw", "*N", { desc = "Search word under cursor" })

--+ -- Moving tabs
--+ keymap("n", "<s-a-l>", "<cmd>BufferMoveNext<cr>", opts) -- Move tab right (shift-alt-l)
--+ keymap("n", "<s-a-h>", "<cmd>BufferMovePrev<cr>", opts) -- Move tab left  (shift-alt-h)
--+ keymap("n", "<s-a-right>", "<cmd>BufferMoveNext<cr>", opts) -- Move tab right
--+ keymap("n", "<s-a-left>", "<cmd>BufferMovePrev<cr>", opts) -- Move tab left
--+ keymap("n", "<s-a-0>", "<cmd>BufferMoveStart<cr>", opts) -- Move tab to the beginning

-- Terminal --
-- Better terminal navigation
-- keymap("t", "<c-h>", "<c-\\><c-n><c-w>h", term_opts)
-- keymap("t", "<c-j>", "<c-\\><c-n><c-w>j", term_opts)
-- keymap("t", "<c-k>", "<c-\\><c-n><c-w>k", term_opts)
-- keymap("t", "<c-l>", "<c-\\><c-n><c-w>l", term_opts)

-- NvimTree
--X keymap("n", "<leader>e", ":NvimTreeToggle<cr>", opts)

-- Telescope
-- keymap("n", "<leader>f", "<cmd>Telescope find_files<cr>", opts)
--X keymap("n", "<leader>f", "<cmd>lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({ previewer = false }))<cr>", opts)
--X keymap("n", "<c-t>", "<cmd>Telescope live_grep<cr>", opts)

--X keymap("n", "<leader>o", ":Format<cr>", opts)

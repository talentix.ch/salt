{% set user = grains.user %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: neovim

# 
# {{ tplfile }}> Install stylua package:
#   cmd.run:
#     - name: yay -S stylua --answerclean None --answerdiff None --answeredit None
#     - runas: {{ user }}
#     - unless: test -f /usr/bin/{{ slspath }}

{{ tplfile }}> Create symlink for {{ slspath }}:
  file.symlink:
  - name: /usr/bin/vim
  - target: /usr/bin/{{ slspath }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  file.recurse:
  - name: /home/{{ user }}/.config/{{ slspath }}
  - source: salt://{{ slspath }}/files/home/user/config/{{ slspath }}
  - makedirs: True
  - user: {{ user }}
  - group: {{ user }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for user root:
  file.recurse:
  - name: /root/.config/{{ slspath }}
  - source: salt://{{ slspath }}/files/home/user/config/{{ slspath }}
  - makedirs: True
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

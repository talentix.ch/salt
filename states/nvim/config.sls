{% set user = grains.user %}

{{ tplfile }}> Replace color code in barbar plugin:
  file.replace:
  - name: /home/{{ user }}/.local/share/nvim.my/lazy/barbar.nvim/lua/barbar/highlight.lua
  - pattern: 'local fg = hl.fg_or_default\(current_hl, "#efefef", 255\)'
  - repl: 'local fg = hl.fg_or_default(current_hl, "#005ebd", 255)'
  - backup: False
  - bufsize: file

#!/bin/sh
### Called from ~/.config/i3/config
xrandr --output DP-2 --primary --mode 3440x1440 --pos 0x0 --rotate normal --output DP-3 --mode 3440x1440 --pos 2560x0 --rotate normal --output DP3 --off --output HDMI1 --off --output HDMI2 --off --output HDMI3 --off --output VIRTUAL1 --off

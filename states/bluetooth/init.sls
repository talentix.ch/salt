# https://www.jeremymorgan.com/tutorials/linux/how-to-bluetooth-arch-linux/

{{ tplfile }}> Install bluetooth packages:
  pkg.installed:
  - pkgs:
      - blueman
      - bluez-utils
      # blueman also installs dependencies: bluez, bluez-libs

{{ tplfile }}> Enable and start bluetooth service and restart on config changes:
  service.running:
  - name: bluetooth
  - enable: True
  - require:
    - {{ tplfile }}> Install bluetooth packages

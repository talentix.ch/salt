{% set user = grains.user %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

{{ tplfile }}> Restore {{ slspath }} config for user {{ user }}:
  cmd.run:
  - name: cp -p /backup/restore-user/.kube /home/{{ user }}/
  - runas: {{ user }}
  - unless: test -d /home/{{ user }}/.kube
  - onlyif: test -d /home/{{ user }} && test -f /backup/restore-user/.kube
  - require:
    - {{ tplfile }}> Installing {{ slspath }}
  - failhard: True

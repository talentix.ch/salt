{% set user = grains.user %}
{% set id   = grains.id   %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for wheel group:
  file.managed:
  - name: /etc/sudoers.d/wheel
  - user: root
  - group: root
  - mode: 440
  - contents: |
      %wheel ALL=(ALL) ALL
      Defaults passwd_timeout=30
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{{ tplfile }}> Configure to prevent lecture message on 1-st sudo usage:
  file.managed:
  - name: /etc/sudoers.d/privacy
  - source: salt://{{ slspath }}/files/etc/sudoers.d/privacy
  - makedirs: True
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{{ tplfile }}> Configure sudo commands for user {{ user }}:
  file.managed:
  - name: /etc/sudoers.d/{{ user }}
  - source: salt://{{ slspath }}/files/etc/sudoers.d/user
  - makedirs: True
  - template: jinja
  - context:
      user: {{ user }}
      id:   {{ id   }}

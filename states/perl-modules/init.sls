{{ tplfile }}> Install perl modules:
  pkg.installed:
  - pkgs:
    - cpanminus
    - perl-io-socket-ssl
    - perl-ldap
#      - perl-net-ldaps
#      - perl-archlinux-module

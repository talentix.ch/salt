{{ tplfile }}> Install audio packages:
  pkg.installed:
  - pkgs:
    - alsa-tools
    - alsa-utils
    - alsa-plugins
    - pulseaudio
    - pulseaudio-alsa
    - pavucontrol
    - pulseaudio-bluetooth

# REBOOT
# TESTING
# aplay -l
# lspci | grep -i audio
# ls -l /dev/snd/
# alsamixer -c 0
# speaker-test
# speaker-test -c 2

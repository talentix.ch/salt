{% set user = grains.user                                                                       %}

{{ tplfile }}> Configure systemd service for ssh-agent for user {{ user }}:
  file.recurse:
  - name: /home/{{ user }}/.config/systemd
  - source: salt://{{ slspath }}/files/home/user/config/systemd
  - makedirs: True
  - user: {{ user }}
  - group: {{ user }}
  - onlyif: test -d /home/{{ user }}/.config

# {{ tplfile }}> Enable user service {{ slspath }}:
#   cmd.run:
#     - name: systemctl --user enable {{ slspath }}
#     - runas: {{ user }}
#     - unless: systemctl --user is-enabled {{ slspath }}
#     - require: 
#       - {{ tplfile }}> Configure systemd service for ssh-agent for user {{ user }}
# 
# {{ tplfile }}> Start user service {{ slspath }}:
#   cmd.run:
#     - name: systemctl --user start {{ slspath }}
#     - runas: {{ user }}
#     - unless: systemctl --user is-active {{ slspath }}
#     - require: 
#       - {{ tplfile }}> Enable user service {{ slspath }}

{% set restore_path_user = grains.restore_path_user %}
{% set restore_path_root = grains.restore_path_root %}
{% set user              = grains.user              %}

{{ tplfile }}> Disable PermitRootLogin with password in ssh_config:
  file.replace:
  - name: /etc/ssh/sshd_config
  - pattern: '^PermitRootLogin yes'
  - repl: 'PermitRootLogin prohibit-password'
#  - bufsize: file

{{ tplfile }}> Restore ssh config for user root:
  cmd.run:
  - name: cp -r {{ restore_path_root }}/.ssh /root/
  - creates: /root/.ssh

{{ tplfile }}> Restore ssh config for user {{ user }}:
  cmd.run:
  - name: cp -r {{ restore_path_user }}/.ssh /home/{{ user }}/
  - creates: /home/{{ user }}/.ssh

#   file.recurse:
#   - name: /root/.ssh
#   - source: salt://files/root/.ssh

#   file.recurse:
#   - name: /home/{{ user }}/.ssh
#   - user: {{ user }}
#   - group: {{ user }}
#   - source: {{ restore_path_user }}/.ssh
# 
# {{ tplfile }}> Restore ssh config for user root:
#   cmd.run:
#   - name: rsync -a /backup/restore-root/.ssh /root/
#   - unless: test -f /root/.ssh/config
#   - failhard: True

{{ tplfile }}> Enable sshd and restart if config has changed:
  service.running:
  - name: sshd
  - enable: True
  - watch:
    - file: /etc/ssh/sshd_config

{{ tplfile }}> Permit PubkeyAuthentication in ssh_config:
  file.replace:
  - name: /etc/ssh/sshd_config
  - pattern: '^#PubkeyAuthentication yes'
  - repl: 'PubkeyAuthentication yes'
#  - bufsize: file

#{{ tplfile }}> Permit PermitRootLogin in ssh_config:
#  file.replace:
#  - name: /etc/ssh/sshd_config
#  - pattern: '^#PermitRootLogin prohibit-password'
#  - repl: 'PermitRootLogin yes'
#  - bufsize: file

# {{ tplfile }}> Permit PasswordAuthentication in ssh_config:
#   file.replace:
#   - name: /etc/ssh/sshd_config
#   - pattern: '^#*PasswordAuthentication no'
#   - repl: 'PasswordAuthentication yes'
#   - bufsize: file

{{ tplfile }}> Disable PasswordAuthentication in ssh_config:
  file.replace:
  - name: /etc/ssh/sshd_config
  - pattern: '^PasswordAuthentication yes'
  - repl: 'PasswordAuthentication no'
#  - bufsize: file

#{{ tplfile }}> Assure ssh service is running:
#  service.running:
#  - name: sshd
#  - enable: True

#{{ tplfile }}> Stop sshd service:
#  service.dead:
#  - name: sshd
#  - enable: False

#{{ tplfile }}> Stop sshd service:
#  service.dead:
#  - name: sshd
#  - enable: False
#  - require:
#    - {{ tplfile }}> Stop sshd service

#{{ tplfile }}> Disable sshd service:
#  service.disabled:
#  - name: sshd
#  - require:
#    - {{ tplfile }}> Stop sshd service

##{{ tplfile }}> mask sshd service:
##  service.masked:
##  - name: systemd-timesyncd
##  - require:
##    - {{ tplfile }}> Disable systemd-timesyncd.service

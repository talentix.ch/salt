#!/usr/bin/env bash
#

# set -uo pipefail
# trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

#===============================================================================
err_exit() {
  echo
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" | grep -P --color ".+"
  echo "!!! ERROR: $1" | grep -P --color ".+"
  echo "!!!        Aborting script!" | grep -P --color ".+"
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" | grep -P --color ".+"
  echo
  exit 1
}



#===============================================================================
# MAIN
#===============================================================================
echo "Running script $0. The following tasks will be done:"
echo " - Clone the saltstack states to complete the archlinux bootstrap setup."
echo " - Ask for a username for the archlinux setup."
echo " - Run a saltstack high-state to apply the saltstack states for the archlinux configuration."
read -p ">>> Press ENTER to continue or Ctrl+C to abort."

# Cloning salt-bootstrap repo
rm -rf /srv/salt
cd /srv
echo " +++ Cloning git@gitlab.com:talentix.ch/salt-bootstrap.git /srv/salt"
git clone git@gitlab.com:talentix.ch/salt-bootstrap.git salt
echo
read -p ">>> Clonig done. Showing files in /srv/salt. Press ENTER to continue."
find /srv/salt
read -p ">>> Press ENTER to continue or Ctrl+C to abort."

cd /srv/salt
echo
read -p ">>> Provide username to be set as saltstack grain variable, then press ENTER to continue or Ctrl+C to abort: " username
echo "+++ Set grain user=$username"
salt-call --local grains.set user $username

# read -p ">>> Press ENTER to continue and run high-state or Ctrl+C to abort."
# salt-call --local state.apply

echo 
echo "+++ Bootstrap installation is done!
 1. Set root password
 2. Exit from chroot
 3. umount -R /mnt
 4. reboot 0
 5. after reboot start /root/install.sh

NOTE:
If applying one or more of the saltstack states has failed, those states
can be corrected and the highstate can be re-applied with the command: 
   salt-call state.apply"
exit 0

# echo "+++ Removing /srv/salt"
cd /srv
# rm -rf /srv/salt

# Mount backup disk
mkdir -p /backup
if dmidecode -s system-manufacturer | grep -q "QEMU"; then
  backup_dev='/backup'
  echo "    +++ We are running in a vm. Mounting backup device $backup_dev to the mountpoint /backup"
  mount -t virtiofs $backup_dev /backup
else
  mount -t xfs backup_device /backup
fi

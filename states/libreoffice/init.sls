{% set user = grains.user %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}-fresh

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  cmd.run:
  - name: rsync -a /backup/restore-user/.config/{{ slspath }} /home/{{ user }}/.config/
  - runas: {{ user }}
  - onlyif: test -d/backup/restore-user/.config/{{ slspath }} 
  - unless: test -d /home/{{ user }}/.config/{{ slspath }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }}
  - failhard: True

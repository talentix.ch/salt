{% set user      = grains.user                     %}
{% set font_size = '13' if user == 'uri' else '10' %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  file.recurse:
  - name: /home/{{ user }}/.config/{{ slspath }}
  - source: salt://{{ slspath }}/files/home/user/config/{{ slspath }}
  - makedirs: True
  - user: {{ user }}
  - group: {{ user }}
  - template: jinja
  - context: 
      font_size: {{ font_size }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }}
    - user: {{ user }}

{{ tplfile }}> Configure {{ slspath }} for user root:
  file.recurse:
  - name: /root/.config/{{ slspath }}
  - source: salt://{{ slspath }}/files/home/user/config/{{ slspath }}
  - makedirs: True
  - template: jinja
  - context: 
      font_size: {{ font_size }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

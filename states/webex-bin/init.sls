{% set user = grains.user %}

# List all installed packages to check dependencies
# LC_ALL=C pacman -Qi | awk '/^Name/{name=$3} /^Installed Size/{print $4$5, name}' | grep -P "alsa-lib|at-spi2-atk|at-spi2-core|atk|binutils|krb5|libcups|libnotify|libpulse|libsecret|libxcb|libxcrypt-compat|libxkbcommon-x11|libxss|mesa|nspr|nss|pango|systemd-libs|upower|wayland|xcb-util-image|xcb-util-keysyms|xcb-util-renderutil|xcb-util-wm|xdg-utils"

{{ tplfile }}> Clone {{ slspath }}:
  git.cloned:
  - name: https://aur.archlinux.org/{{ slspath }}.git
  - user: {{ user }}
  - target: /home/{{ user }}/.local/sources/{{ slspath }}
  - creates: /home/{{ user }}/.local/sources/{{ slspath }}

{{ tplfile }}> Make and install package {{ slspath }}:
  cmd.run:
  - name: makepkg -sicC --noconfirm
  - runas: {{ user }}
  - cwd: /home/{{ user }}/.local/sources/{{ slspath }}
  - require:
    - {{ tplfile }}> Clone {{ slspath }}
  - onlyif: test -d '/home/{{ user }}/.local/sources/{{ slspath }}'
  - unless: test -f /usr/bin/webex

{{ tplfile }}> Cleanup installation tar ball files from /home/{{ user }}/.local/sources/{{ slspath }}:
  file.absent:
  - names:
    - /home/{{ user }}/.local/sources/{{ slspath }}/webex-bin-43.2.0.25211-1-x86_64.pkg.tar.zst
    - /home/{{ user }}/.local/sources/{{ slspath }}/webex-bin-43.2.0.25211.deb
    - /home/{{ user }}/.local/sources/{{ slspath }}/libsecret-0.20.4-1-x86_64.pkg.tar.zst
  - require:
    - {{ tplfile }}> Make and install package {{ slspath }}

#{{ tplfile }}> Installing {{ slspath }}:
#  cmd.run:
#    - name: yay -Sc {{ slspath }} --answerclean None --answerdiff None --answeredit None --answerupgrade None
#    - runas: {{ user }}
#    - unless: test -f /usr/bin/webex

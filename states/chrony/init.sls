{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

{{ tplfile }}> Provide {{ slspath }} ntp sources {{ slspath }}/files/etc/chrony.d:
  file.recurse:
  - name: /etc/chrony.d
  - source: salt://{{ slspath }}/files/etc/chrony.d
  - makedirs: True
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{{ tplfile }}> Set default ntp server to comment:
  file.replace:
  - name: /etc/chrony.conf
  - pattern: '^(pool.+iburst)'
  - repl: '# \1'
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{{ tplfile }}> Add include of config directory:
  cmd.run:
  - name: | 
      echo "#######################################################################">> /etc/{{ slspath }}.conf
      echo "### INCLUDE MORE CONFIGS" >> /etc/{{ slspath }}.conf
      echo "include /etc/{{ slspath }}.d/*.conf" >> /etc/{{ slspath }}.conf
  - unless: grep -q "include /etc/{{ slspath }}.d/\*\.conf" /etc/{{ slspath }}.conf
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

# {{ tplfile }}> Add include of config directory:
#   file.append:
#   - name: /etc/{{ slspath }}.conf
#   - text: |
#       #######################################################################
#       ### INCLUDE MORE CONFIGS
#       include /etc/{{ slspath }}.d/*.conf
#   - require:
#     - {{ tplfile }}> Installing {{ slspath }}

# {{ tplfile }}> Configure chrony ntp source:
#   file.replace:
#   - name: /etc/chrony.conf
#   - pattern: '^(pool.+iburst)'
#   - repl: '# \1\n\nserver ntp.metas.ch iburst\n\n! server ntp11.metas.ch iburst\n! server ntp12.metas.ch iburst\n! server ntp13.metas.ch iburst\n! server 0.ch.pool.ntp.org iburst'
#     - bufsize: file
#     - require:
#       - {{ tplfile }}> Install chrony package

{{ tplfile }}> Enable and start {{ slspath }} ntpd service and restart on config changes:
  service.running:
  - name: chronyd
  - enable: True
  - watch:
    - file: /etc/chrony.conf
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{{ tplfile }}> Stop systemd-timesyncd.service:
  service.dead:
  - name: systemd-timesyncd
  - enable: False
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{{ tplfile }}> Disable systemd-timesyncd.service:
  service.disabled:
  - name: systemd-timesyncd
  - require:
    - {{ tplfile }}> Stop systemd-timesyncd.service

{{ tplfile }}> mask systemd-timesyncd.service:
  service.masked:
  - name: systemd-timesyncd
  - require:
    - {{ tplfile }}> Disable systemd-timesyncd.service

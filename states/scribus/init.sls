{% set user = grains.user %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

# {{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
#   file.recurse:
#     - name: /home/{{ user }}/.config/{{ slspath }}
#     - source: salt://{{ slspath }}/files/home/user/config/{{ slspath }}
#     - makedirs: True
#     - user: {{ user }}
#     - group: {{ user }}
#     - require:
#       - {{ tplfile }}> Installing {{ slspath }}
#       - user: {{ user }}

# {{ tplfile }}> Restore {{ slspath }} data for user {{ user }}:
#   cmd.run:
#     - name: rsync -a /backup/restore-user/.local /home/{{ user }}/
#     - onlyif: test -d /backup/restore-user/.local/share/{{ slspath }}
#     - unless: test -d /home/{{ user }}/.local/share/{{ slspath }}
#     - require:
#       - {{ tplfile }}> Installing {{ slspath }}
#    - failhard: True

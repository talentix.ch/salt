# Linux: US International keyboard layout
Thiago Perrotta

I use QWERTY keyboards with a US layout. Sometimes I need to type accents or cedillas, and I keep forgetting how to do so, this post summarizes how to do it.


## Intro

There are basically two layouts:

 - US (‘vanilla’): type accents like '^`~ and they will be emitted immediately
 - US International (INTL): accents are the so called ‘dead keys’:

A dead key is a special kind of a modifier key on a mechanical typewriter, or computer keyboard, that is typically used to attach a specific diacritic to a base letter.

We can switch between keyboard layouts with setxkbmap. It’s also possible to use localectl in systemd-based distros, but its syntax is harder to remember so I won’t
even include it here.

## Set US ‘vanilla’ keyboard layout

```code
setxkbmap us
```

This is what a standard QWERTY keyboard should use to type in English.
Set US International (INTL) keyboard layout

```code
setxkbmap -layout us -variant intl
```

This is what a standard QWERTY keyboard1 should use to type, for example, in Portuguese or in German.

Portuguese
- á é í ó ú     :   ' + <vowel>
- â ê î ô û     :   ^ + <vowel>
- ã õ           :   ~ + <vowel>
- à             :   ` + <vowel>
- ç (cedilla)   :   Alt Gr + , (Option + c on macOS)

German
- ß (ss)  :   Alt Gr + s (Option + s on macOS)
- ä ö ü   :   " + <vowel>


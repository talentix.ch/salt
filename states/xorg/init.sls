{% set id = 'qemu' if grains.manufacturer == 'QEMU' else grains.id %}
# STARTX WITH XTERM
# startx

# KILL XTERM
# Ctrl+Alt+F2
# sudo kill xterm

{{ tplfile }}> Installing xorg packages:
  pkg.installed:
  - pkgs:
    - xorg-server
    - xorg-xsetroot
    - xorg-xkill
    - xorg-xbacklight
    - xf86-input-synaptics
    - xf86-input-libinput
    - xorg-xdpyinfo
    - xorg-xhost
    - xorg-xinit
    - numlockx
    - xterm
    - arandr
    - mesa
#      - xorg-xsetroot

{% if id == 'clt-dsk-v-7133' %}
#{{ tplfile }}> Installing xorg video adapter card packages for system {{ id }}:
#  pkg.installed:
#    - pkgs:
#      - xf86-video-nouveau
#      - nvidia
#      - nvidia-utils
#      - nvidia-settings
#      #- opencl-nvidia
{% endif %}

{% if id == 'talentix' %}
{{ tplfile }}> Installing xorg video adapter card packages for system {{ id }}:
  pkg.installed:
  - pkgs:
    - xf86-video-amdgpu
{% endif %}

{% if id == 'qemu' %}
# {{ tplfile }}> Installing xorg video adapter card packages for system {{ id }}:
#   pkg.installed:
#     - pkgs:
#       - xf86-video-amdgpu
{% endif %}

{{ tplfile }}> Configure keyboard:
  file.managed:
  - name: /etc/X11/xorg.conf.d/00-keyboard.conf
  - source: salt://{{ slspath }}/files/etc/X11/xorg.conf.d/00-keyboard.conf
  - require:
    - {{ tplfile }}> Installing xorg packages

{{ tplfile }}> Configure display resolution:
  file.managed:
  - name: /etc/X11/xorg.conf.d/10-monitor.conf
  - source: salt://{{ slspath }}/files/etc/X11/xorg.conf.d/10-monitor.conf.{{ id }}
  - require:
    - {{ tplfile }}> Installing xorg packages

# {{ tplfile }}> Set mouse cursor:
#   file.replace:
#     - name: /usr/share/icons/default/index.theme
#     - pattern: "^Inherits=Adwaita"
#     - backup: False
#     - repl: "# Inherits=Adwaita"
#     - bufsize: file
#     - require:
#       - {{ tplfile }}> Installing xorg packages

# (1/1) installing xf86-video-intel                                                                                               [#############################################################################] 100%
# >>> This driver now uses DRI3 as the default Direct Rendering
#     Infrastructure. You can try falling back to DRI2 if you run
#     into trouble. To do so, save a file with the following
#     content as /etc/X11/xorg.conf.d/20-intel.conf :
#       Section "Device"
#         Identifier  "Intel Graphics"
#         Driver      "intel"
#         Option      "DRI" "2"             # DRI3 is now default
#         #Option      "AccelMethod"  "sna" # default
#         #Option      "AccelMethod"  "uxa" # fallback
#       EndSection

#<< {# for gpu in grains.gpus        #}
#<< {#   if gpu.vendor == 'intel'    #}
#<<       - xf86-video-intel
#<< {#   elif gpu.vendor == 'nvidia' #}
#<<       - xf86-video-intel
#<<       - nvidia
#<< {#   else                        #}
#<<       - xf86-video-vesa
#<< {#   endif                       #}
#<< {# endfor                        #}

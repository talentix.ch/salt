{% set user = grains.user | default('uri') %}

{{ tplfile }}> Mount data volume to data folder:
  mount.mounted:
  - name: /home/{{ user }}/data
  - device: UUID=515b1bb5-2a93-4364-95b4-0359c7a078fa
  - fstype: xfs
  - opts: rw,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota
  - dump: 0
  - pass_num: 2
  - persist: True
  - mkmnt: True
  - onlyif: test -d /home/{{ user }}

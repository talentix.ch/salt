{% set user = grains.user %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  file.managed:
  - name: /home/{{ user }}/.fzf.zsh
  - source: salt://{{ slspath }}/files/home/user/.fzf.zsh
  - user: {{ user }}
  - group: {{ user }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }}
    - pkg: zsh
    - user: {{ user }}

{{ tplfile }}> Configure {{ slspath }} for user root:
  file.managed:
  - name: /root/.fzf.zsh
  - source: salt://{{ slspath }}/files/root/.fzf.zsh
  - require:
    - {{ tplfile }}> Installing {{ slspath }}
    - pkg: zsh

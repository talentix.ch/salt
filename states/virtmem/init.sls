{{ tplfile }}> Increase kernel parameter for virtual memory for games and other applications:
  sysctl.present:
  - name: vm.max_map_count
  - value: 2147483642 
  - config: /etc/sysctl.d/90-virtmem.conf

# NOTE: Alternatively you could just provide a config file that contains the requred settings
# {{ tplfile }}> Increase kernel parameter for virtual memory for games and other applications:
#   file.managed:
#   - name: /etc/sysctl.d/90-virtmem.conf
#   - source: salt://{{ slspath }}/files/etc/sysctl.d/90-virtmem.conf

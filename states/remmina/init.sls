{% set user = grains.user %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - pkgs:
    - freerdp
    - remmina

{{ tplfile }}> Restore {{ slspath }} config for user {{ user }}:
  cmd.run:
  - name: rsync -a /backup/restore-user/.config/{{ slspath }} /home/{{ user }}/.config/
  - onlyif: test -d /backup/restore-user/.config/{{ slspath }}
  - unless: test -d /home/{{ user }}/.config/{{ slspath }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }}


{% set plugin = 'remmina-plugin-rdesktop' %}

{{ tplfile }}> Clone {{ plugin }}:
  git.cloned:
  - name: https://aur.archlinux.org/{{ plugin }}.git
  - user: {{ user }}
  - target: /home/{{ user }}/.local/sources/{{ plugin }}
  - creates: /home/{{ user }}/.local/sources/{{ plugin }}

{{ tplfile }}> Make and install package {{ plugin }}:
  cmd.run:
  - name: makepkg -sicC --noconfirm
  - runas: {{ user }}
  - cwd: /home/{{ user }}/.local/sources/{{ plugin }}
  - require:
    - {{ tplfile }}> Clone {{ plugin }}
  - onlyif: test -d '/home/{{ user }}/.local/sources/{{ plugin }}'
  - unless: test -f /usr/lib/remmina/plugins/remmina-plugin-rdesktop.so

{{ tplfile }}> Restore {{ plugin }} config for user {{ user }}:
  cmd.run:
  - name: rsync -a /backup/restore-user/.config/{{ plugin }} /home/{{ user }}/.config/
  - onlyif: test -d /backup/restore-user/.config/{{ plugin }}
  - unless: test -d /home/{{ user }}/.config/{{ plugin }}
  - require:
    - {{ tplfile }}> Make and install package {{ plugin }}
  - failhard: True

{{ tplfile }}> Cleanup installation tar ball files from /home/{{ user }}/.local/sources/{{ plugin }}:
  file.absent:
  - names:
    - /home/{{ user }}/.local/sources/{{ plugin }}/remmina-plugin-rdesktop-1.3.0.0-3-x86_64.pkg.tar.zst
    - /home/{{ user }}/.local/sources/{{ plugin }}/remmina-plugin-builder_1.4.27.0.tar.gz
    - /home/{{ user }}/.local/sources/{{ plugin }}/remmina-plugin-rdesktop_1.3.0.0.tar.gz
  - require:
    - {{ tplfile }}> Make and install package {{ plugin }}

# {{ tplfile }}> Install remmina-plugin-rdesktop:
#   cmd.run:
#     - name: yay -S remmina-plugin-rdesktop --answerclean None --answerdiff None --answeredit None
#     - runas: {{ user }}
#     - require:
#       - {{ tplfile }}> Installing {{ slspath }}
#     - unless: test -f /usr/lib/remmina/plugins/remmina-plugin-spice.so

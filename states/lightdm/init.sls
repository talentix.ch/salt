{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - pkgs:
    - lightdm
    - lightdm-gtk-greeter

# https://wiki.archlinux.org/title/Activating_numlock_on_bootup
{{ tplfile }}> Turn on numlock:
  file.replace:
  - name: /etc/lightdm/lightdm.conf
  - pattern: "^#greeter-setup-script="
  - repl: "greeter-setup-script=/usr/bin/numlockx on"
  - backup: False
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{{ tplfile }}> Delete unneccessary apparmor config:
  file.absent:
  - name: /etc/apparmor.d

{{ tplfile }}> Enable lightdm and restart if config has changed:
  service.running:
  - name: lightdm
  - enable: True
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

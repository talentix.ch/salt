{% set user = grains.user %}

{{ tplfile }}> Installing {{ slspath }} tools:
  pkg.installed:
  - pkgs:
    - qemu-system-x86
    - qemu-system-x86-firmware
    - qemu-base
    - qemu-common
    - qemu-img
    - qemu-desktop
    - qemu-tests
    - qemu-tools
    - qemu-user
    - qemu-ui-spice-core
    - qemu-ui-spice-app
    - qemu-ui-opengl
    - qemu-ui-gtk
    - qemu-ui-dbus
    - qemu-audio-alsa
    - qemu-audio-dbus
    - qemu-audio-oss
    - qemu-audio-spice
#     - qemu-virtiofsd
#     - qemu-guest-agent
    - libvirt
    - virt-manager
    - virt-viewer
    - virt-install
    - iptables
    - dnsmasq
    - openbsd-netcat
    - vde2
    - bridge-utils
    - edk2-ovmf

# {{ tplfile }}> Add {{ user }} to groups kvm and libvirt:
#   user.present:
#     - name: {{ user }}
#     - groups:
#       - kvm
#       - libvirt
#       - qemu
#     - remove_groups: False
#     - require:
#       - {{ tplfile }}> Installing {{ slspath }} tools

{{ tplfile }}> Add {{ user }} to group kvm:
  group.present:
  - name: kvm
  - addusers:
    - {{ user }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }} tools

# {{ tplfile }}> Add {{ user }} to group libvirt:
#   group.present:
#     - name: libvirt
#     - addusers:
#       - {{ user }}
#     - require:
#       - {{ tplfile }}> Installing {{ slspath }} tools

{{ tplfile }}> Configure timezone:
  file.symlink:
  - name: /etc/localtime
  - target: /usr/share/zoneinfo/Europe/Zurich

#{{ tplfile }}> Manage timezone:
#  timezone.system:
#  - name: Europe/Zurich
#  - require:
#    - {{ tplfile }}> Configure timezone

# {{ tplfile }}> Create /etc/adjtime:
#   cmd.run:
#   - name: hwclock --systohc
#   - creates:
#     - /etc/adjtime
#   - require:
#     - {{ tplfile }}> Configure timezone

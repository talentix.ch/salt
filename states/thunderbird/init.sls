{% set user = grains.user %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  cmd.run:
  - name: rsync -a /backup/restore-user/.{{ slspath }} /home/{{ user }}/
  - runas: {{ user }}
  - unless: test -d /home/{{ user }}/.{{ slspath }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }}
  - failhard: True

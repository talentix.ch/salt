{% set user = grains.user %}

{{ tplfile }}> Restore bin directory for user {{ user }}:
  cmd.run:
  - name: rsync -a /backup/restore-user/bin /home/{{ user }}/
  - onlyif: test -d /backup/restore-user/bin
  - unless: test -d /home/{{ user }}/bin

{{ tplfile }}> Restore git directory for user {{ user }}:
  cmd.run:
  - name: rsync -a /backup/restore-user/git /home/{{ user }}/
  - onlyif: test -d /backup/restore-user/git
  - unless: test -d /home/{{ user }}/git

{{ tplfile }}> Restore ksnip directory for user {{ user }}:
  cmd.run:
  - name: rsync -a /backup/restore-user/ksnip /home/{{ user }}/
  - onlyif: test -d /backup/restore-user/ksnip
  - unless: test -d /home/{{ user }}/ksnip

{{ tplfile }}> Restore Audio directory for user {{ user }}:
  cmd.run:
  - name: rsync -a /backup/restore-user/Audio /home/{{ user }}/
  - onlyif: test -d /backup/restore-user/Audio
  - unless: test -d /home/{{ user }}/Audio

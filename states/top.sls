#%%      - firmware

base:
  '*':
# Saltstack to use it as early as possible and have grains prepared for restoring user data
  - salt-minion
  - salt-master
  - check-restore-paths-of-user-data

# OS and system components
  - network
  - repo
  - timezone
  - locale
  - cronie
  - chrony
  - man-pages
  - man-db
  - virtmem

# General system tools
  - ssh
  - sudo
  - lshw
  - lsof
  - xfsprogs
  - cifs-utils
  - dosfstools
  - pciutils
  - usbutils
  - audio
  - htop
  - file
  - which
  - plocate
  - fd
  - wget
  - libcurl-compat
  - whois
  - grep
  - ripgrep
  - ack
  - sed
  - gzip
  - p7zip
  - unzip
  - rsync
  - gawk
  - diff-so-fancy
  - findutils
  - binutils
  - stow
  - bison
  - flex
  - gettext
  - texinfo
  - pacman-contrib
  - bash-completion
  - perl-modules

# File viewers
  - less
  - mpv
  - feh
  #- zathura
  - imagemagick

# File managers
  - vifm
  - nemo             # w/o config  .config/nemo
  - pcmanfm          # w/o config ~/.config/pcmanfm
  - dolphin          # w/o .config/dolphinrc, .local/share/dolphin .config/session/dolphin... /usr/share/config.kcfg/dohphin...

# PDF tools
  - pdfgrep
  - mupdf
  - qpdf
  - pdfarranger
  - pdftk

# MD tools
  - glow

# Development tools
  - git
  - tig
  - gcc
  - make
  - automake
  - autoconf
  - fakeroot
  - pkgconf
  - libtool
  - patch
  - m4
  - tcl
  - tk

  - jq


# Misc utils
  - bc
  - yt-dlp
  - xclip
  - ldap
  - sqlite           # for nvim plugin

  - cups
# VPN tools
  - openvpn          # w/o config
  - openconnect      # w/o config https://wiki.archlinux.org/title/OpenConnect

  - user-config      # ~/.ssh /etc/sudoers /etc/sudo.conf /etc/sudoers.d
#$$ #%%      - vivid            # ~/.config/nvim ~/.local/state/nvim
#$$ #%%      - zsh              # /backup
#$$ #%%      - zprezto          # /backup ~/.zprezto
#$$ #%%      - ssh-agent        # requires user
#$$ #%%      - keepassxc        # /backup  ~/.config/keepass
#$$ #%%      - nvim             # ~/.config/nvim ~/.local/state/nvim
#$$ #%%      - nnn              # /backup ~/.config/nnn/
#$$ #%%      - kate             # ~/.config/kate/{katerc,katevirc,kate/}
#$$ #%% #     - exa             # aliases in ~/.zshrc
#$$ #%% #     - bat
#$$ #%%      - vlc              # ~/.config/vlc/ remove: list=file: from ~/.config/vlc/vlc-qt-interface.conf
#  - docker           # w/o config ~/.docker /etc/docker ~/.config/Docker Desktop/
#$$ #%%      - fonts            # pacman and aur
#$$ #%%      - kitty            # ~/.config/kitty/
#$$ #%%      - fzf              # ~/.fzf.zsh
#$$ #%%      - xorg             # ~/.x*
#$$ #%%      - kvm              # ??
#$$ #%%      - strawberry       # .config/strawberry .local/strawberry/strawberry.db
#$$ #%%      - ksnip            # .config/ksnip
#$$ #%% #     - nomacs           # w/o config image viewer ./config/nomacs
#$$ #%%      - scribus          # w/o config .config/scribus .local/share/scribus
#$$ #%%      - shotwell         # /backup  .local/share/shtowell
#$$ #%%      - chromium         # /backup  ./config/chromium
#$$ #%%      - firefox          # /backup   .mozilla/firefox
#$$ #%%      - thunderbird      # /backup   .thunderbird
#$$ #%%      - libreoffice      # /backup   .config/libreoffice
#$$ #%%      - obsidian         # w/o config .config/obsidian
#$$ #%%      - anyconnect       # /backup   .anyconnect /opt/cisco/anyconnect/profile/
#$$ #%%      - kubectl
#$$ #%%      - k9s              # .config/k9s
#$$ #%%      - yay              # yay
#$$ #%%      - logiops          # w/o config yay
#$$ #%%      - zplug            # w/o config yay
#$$ #%%      - webex-bin        # w/o config yay
#$$ #%%      - remmina          # pacman & yay
#$$ #%%      - teamviewer       # yay .config/teamviewer
#$$ #%%      - timeshift        # yay .config/teamviewer
#$$ #%%      - userdata         # ~/userdata from /backup
#$$ #%%      - i3               # ~/.config/i3/
#$$ #%%      - dmenu            # ~/.dmrc
#$$ #%%      - lightdm          # rm /etc/apparmor.d/ /etc/lightdm/{Xsession,lightdm.conf
#$$ #%%      - umount-backup    # For vms only
#$$ #%% #     - updates
#$$ #%% #>>
#$$ #%% #>> # ssh-add, ssh-agent  Slickedit ENV VAR
#$$ #%% #>> #>> #=     - ssh                cfg
#$$ #%% #>> #>> #=     - vpn                cfg
#$$ #%% #>> #    - ntfs-3g
#$$ #%% #>>
#$$ #%% #>> ###=  pacman -Q firefox.desktop
#$$ #%% #>> ###=  pacman -Q default-web-browser
#$$ #%% #>> #=    pacman -Q xdg-settings
#$$ #%% #>>
#$$ #%% #>> {% if grains.manufacturer != 'QEMU' %}
#$$ #%% #>>      - bluetooth         # w/o config
#$$ #%% #>>      - backintime        # yay /backup
#$$ #%% #>> {% endif                            %}
#$$ #%% #>>
#$$ #%% #>> {% if grains.serialnumber == '18Y4HR3' %}
#$$ #%% #>>      - mount-data
#$$ #%% #>>      - hplip
#$$ #%% #>> {% endif %}
#$$ #%% #>>
#$$ #%% #>> {% if grains.serialnumber == 'BMBH7W3' %}
#$$ #%% #>> #     - wpa-supplicant
#$$ #%% #>> {% endif %}
#$$ #%% #>>
#$$ #%% #>>
#$$ #%% #>> # More configs in ~/.config/dconf/
#$$ #%% #>> # sudo pacman -S dconf-editor
#$$ #%% #>> # dconf list /org/nemo/
#$$ #%% #>> # dconf list /org/nemo/window-state/
#$$ #%% #>> # dconf read /org/nemo/window-state/geometry
#$$ #%% #>> # dconf list /org/
#$$ #%% #>> # dconf list /org/virt-manager/
#$$ #%% #>> # dconf list /org/virt-manager/virt-manager/
#$$ #%% #>> # dconf list /org/virt-manager/virt-manager/connections/
#$$ #%% #>> # dconf read /org/virt-manager/virt-manager/connections/uris
#$$ #%% #>> # dconf read /org/virt-manager/virt-manager/connections/autoconnect
#$$ #%% #>> # dconf dump / >dconf.backup
#$$ #%% #>> # less dconf.backup

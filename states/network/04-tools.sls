{{ tplfile }}> Install network tools:
  pkg.installed:
  - pkgs:
    - bind
    - iproute2
    - iperf3
    - nmap
    - openbsd-netcat
    - inetutils
    - wireshark-qt

{% set id = 'qemu' if grains.manufacturer == 'QEMU' else grains.id %}

{{ tplfile }}> Install nss-mdns packages for multicast dns:
  pkg.installed:
  - pkgs:
    - avahi
    - nss-mdns

{{ tplfile }}> Configure /etc/nsswitch.conf:
  file.managed:
  - name: /etc/nsswitch.conf
  - source: salt://{{ slspath }}/files/etc/nsswitch.conf.{{ id }}
  - require:
    - {{ tplfile }}> Install nss-mdns packages for multicast dns

{{ tplfile }}> Configure dns servers /etc/systemd/resolved.conf:
  file.managed:
  - name: /etc/systemd/resolved.conf
  - source: salt://{{ slspath }}/files/etc/systemd/resolved.conf.{{ id }}
  - require:
    - {{ tplfile }}> Install nss-mdns packages for multicast dns

{{ tplfile }}> Configure systemd-resolved to use stub-resolv.conf:
  file.symlink:
  - name: /etc/resolv.conf
  - target: /run/systemd/resolve/stub-resolv.conf
  - force: True
  - require:
    - {{ tplfile }}> Install nss-mdns packages for multicast dns

{{ tplfile }}> Enable systemd-resolved and restart if config has changed:
  service.running:
  - name: systemd-resolved
  - enable: True
  - watch:
    - file: /etc/resolv.conf
  - require:
    - {{ tplfile }}> Install nss-mdns packages for multicast dns

# Check resolver configuration
# cat /etc/resolv.conf
# cat /run/systemd/resolve/resolv.conf
# cat /run/systemd/resolve/stub-resolv.conf
# cat /usr/lib/systemd/resolv.conf
# systemd-resolve --status
# resolvectl status

# Using resolv.conf instead of stub-resolv.conf will bypass a lot of systemd-resolved configuration,
# such as DNS answer caching, per-interface DNS configuration, DNSSec enforcement, etc.
#
# Explanations:
# When using stub-resolv.conf, applications will make DNS requests to the DNS stub resolver
# provided by systemd on address 127.0.0.53. This stub resolver will proxy the DNS requests
# to the upstream DNS resolvers configured in systemd-resolved, applying whatever logic it
# wants to those requests and answers, like caching them.
#
# When using resolv.conf, applications will directly make DNS requests to the "real"
# (aka. upstream) DNS resolvers configured in systemd-resolved. In this case,
# systemd-resolved only acts as a "resolv.conf manager", not as a DNS resolver itself.


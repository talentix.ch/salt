{% set hostname = grains.id %}

# {{ tplfile }}> Create /etc/hosts:
#   file.managed:
#   - name: /etc/hosts
#   - source: salt://{{ slspath }}/files/etc/hosts
#   - template: jinja
#   - defaults:
#       hostname: {{ hostname }}
# #  - creates: /etc/hosts

# {% if grains.manufacturer != 'QEMU' %}
# {{ tplfile }}> Restore /etc/hosts:
#   cmd.run:
#   - name: cp /backup/restore-etc/hosts /etc/
#   - unless: grep -qi {{ hostname }} /etc/hosts
#   - failhard: True
# {% endif %}    

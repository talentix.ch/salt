{% set network_conf = '20-wired.network'                                           %}
{% set ipv6_conf    = '40-ipv6.conf'                                               %}

# Init artificial dict for passing variable value outside of the scope of a for loop
# https://fabianlee.org/2016/10/18/saltstack-setting-a-jinja2-variable-from-an-inner-block-scope/

{% set wired_network_interface = ({ 'name': 'UNKNOWN' })                           %}

# Get network interfaces from saltstack module
{% set network_interfaces = salt.network.interfaces()                              %}

# Test manually: salt-call network.interfaces or: salt-call network.interface enp1s0
# Loop through all interfaces
{% for network_interface, data in network_interfaces.items()                       %}

# Identify and save wired network interface, saving to the artificial dict
{%   if network_interface.startswith('enp') or network_interface.startswith('eno') %}
{%     if wired_network_interface.update({'name': network_interface }) %} {% endif %}
{%   endif %}
{% endfor %}

{% set wired_network_interface_name = wired_network_interface.name                 %}

#{{ tplfile }}> Noise {{ slspath }} here sls {{ sls }}:
#  cmd.run:
#  - name: |
#      echo {{ slspath }}/files/etc/systemd/network/{{ network_conf }}
#      echo {{ slspath }}/files/etc/sysctl.d/{{ ipv6_conf }}

{{ tplfile }}> Create wired network config with static ip address /etc/systemd/network/{{ network_conf }}:
  file.managed:
  - name: /etc/systemd/network/20-wired.network
  - source: salt://{{ slspath }}/files/etc/systemd/network/{{ network_conf }}.{{ wired_network_interface_name }} 
  - template: jinja
  - defaults:
      wired_network_interface_name: {{ wired_network_interface_name }}
#  - creates: /etc/systemd/network/{{ network_conf }}

{{ tplfile }}> Remove default wired network config /etc/systemd/network/20-ethernet.network:
  file.absent:
  - name: /etc/systemd/network/20-ethernet.network

{{ tplfile }}> Disable ipv6 /etc/sysctl.d/{{ ipv6_conf }}:
  file.managed:
  - name: /etc/sysctl.d/{{ ipv6_conf }}
  - source: salt://{{ slspath }}/files/etc/sysctl.d/{{ ipv6_conf }}
  - template: jinja
  - defaults:
      wired_network_interface_name: {{ wired_network_interface_name }}
#  - creates: /etc/sysctl.d/{{ ipv6_conf }}

{{ tplfile }}> Enable systemd-networkd and restart if config has changed:
  service.running:
  - name: systemd-networkd
  - enable: True
  - watch:
     - file: /etc/systemd/network/{{ network_conf }}
     - file: /etc/sysctl.d/{{ ipv6_conf }}

{% set user = grains.user %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

{{ tplfile }}> Restore wpa-supplicant configuration:
  cmd.run:
  - name: rsync -a /backup/restore-etc/wpa_supplicant /etc/
  - unless: grep -qi {{ hostname }} /etc/wpa_supplicant
  - failhard: True

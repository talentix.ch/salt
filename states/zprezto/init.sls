{% set user = grains.user %}
# Notes: 
# 1. cd ~/git/talentix/salt/states/zprezto/files/home/user/
# 2. mv .zprezto .zpreto.saved
# 3. git clone --recursive https://github.com/sorin-ionescu/prezto.git .zprezto
# 4. rm -rf .zprezto/.git
# 4. cp .zprezto.saved/runcoms/zprofile .zprezto/runcoms/ 
# 5. cp .zprezto.saved/runcoms/zshenv .zprezto/runcoms/ 
# 6. rm -rf /home/$USERNAME/.zprezto /root/.zprerzto 
# 7. Add and commit changes to salt repo
# 8. git pull; salt-call state.apply

{{ tplfile }}> Installing {{ slspath }} for user {{ user }}:
  file.recurse:
  - name: /home/{{ user }}/.{{ slspath }}
  - source: salt://{{ slspath }}/files/home/user/{{ slspath }}
  - makedirs: True
  - user: {{ user }}
  - group: {{ user }}
  - exclude_pat:
    - zshrc.custom
  - creates: /home/{{ user }}/.{{ slspath }}/init.zsh
  - require:
    - pkg: zsh

{{ tplfile }}> Installing {{ slspath }} for user root:
  file.recurse:
  - name: /root/.{{ slspath }}
  - source: salt://{{ slspath }}/files/home/user/{{ slspath }}
  - makedirs: True
  - creates: /root/.{{ slspath }}/init.zsh
  - exclude_pat:
    - zshrc.custom
  - require:
    - pkg: zsh

{{ tplfile }}> Install zshrc for user {{ user }}:
  file.managed:
  - name: /home/{{ user }}/.{{ slspath }}/runcoms/zshrc.custom
  - source: salt://{{ slspath }}/files/home/user/{{ slspath }}/runcoms/zshrc.custom
  - user: {{ user }}
  - group: {{ user }}
  - require:
    - pkg: zsh

{{ tplfile }}> Install zshrc for user root:
  file.managed:
  - name: /root/.{{ slspath }}/runcoms/zshrc.custom
  - source: salt://{{ slspath }}/files/home/user/{{ slspath }}/runcoms/zshrc.custom
  - require:
    - pkg: zsh

{{ tplfile }}> Configure standard editor for user {{ user }}:
  file.replace:
  - name: /home/{{ user }}/.zprezto/runcoms/zprofile
  - backup: False
  - pattern: "'nano'"
  - repl: "'nvim'"
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user {{ user }}

{{ tplfile }}> Configure standard editor for user root:
  file.replace:
  - name: /root/.zprezto/runcoms/zprofile
  - backup: False
  - pattern: "'nano'"
  - repl: "'nvim'"
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user {{ user }}

# {{ tplfile }}> Configure time format yyyy-dd-tt for user {{ user }}:
#   file.replace:
#     - name: /home/{{ user }}/.zprezto/runcoms/zprofile
#     - backup: False
#     - pattern: "(# Language\n#\n)"
#     - repl: \1\nexport LC_TIME="en_DK.UTF-8"\n
#     - bufsize: file
#     - unless: grep -q "en_DK.UTF-8" /home/{{ user }}/.zprezto/runcoms/zprofile
#     - require:
#       - {{ tplfile }}> Installing {{ slspath }} for user {{ user }}
# 
# {{ tplfile }}> Configure time format yyyy-dd-tt for user root:
#   file.replace:
#     - name: /root/.zprezto/runcoms/zprofile
#     - backup: False
#     - pattern: "(# Language\n#\n)"
#     - repl: \1\nexport LC_TIME="en_DK.UTF-8"\n
#     - bufsize: file
#     - unless: grep -q "en_DK.UTF-8" /root/.zprezto/runcoms/zprofile
#     - require:
#       - {{ tplfile }}> Installing {{ slspath }} for user root

{{ tplfile }}> Configure program search path for user {{ user }}:
  file.replace:
  - name: /home/{{ user }}/.zprezto/runcoms/zprofile
  - backup: False
  - pattern: \s+/opt/{homebrew,local.+
  - repl: ""
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user {{ user }}

{{ tplfile }}> Configure program search for user root:
  file.replace:
  - name: /root/.zprezto/runcoms/zprofile
  - backup: False
  - pattern: /opt/{homebrew,local.+
  - repl: i""
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user root

{{ tplfile }}> Disable interactive mode for commands (safe-ops) for user {{ user }}:
  file.replace:
  - name: /home/{{ user }}/.zprezto/runcoms/zpreztorc
  - backup: False
  - pattern: "^# zstyle ':prezto:module:utility' safe-ops 'yes'"
  - repl: "zstyle ':prezto:module:utility' safe-ops 'no'"
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user {{ user }}

{{ tplfile }}> Disable interactive mode for commands (safe-ops) for user root:
  file.replace:
  - name: /root/.zprezto/runcoms/zpreztorc
  - backup: False
  - pattern: "^# zstyle ':prezto:module:utility' safe-ops 'yes'"
  - repl: "zstyle ':prezto:module:utility' safe-ops 'no'"
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user root

{{ tplfile }}> Configure {{ slspath }} prompt config for user {{ user }}:
  file.replace:
  - name: /home/{{ user }}/.zprezto/runcoms/zpreztorc
  - backup: False
  - pattern: "^zstyle ':prezto:module:prompt' theme 'sorin'"
  - repl: "zstyle ':prezto:module:prompt' theme 'skwp'"
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user {{ user }}

{{ tplfile }}> Configure {{ slspath }} prompt config for user root:
  file.replace:
  - name: /root/.zprezto/runcoms/zpreztorc
  - backup: False
  - pattern: "^zstyle ':prezto:module:prompt' theme 'sorin'"
  - repl: "zstyle ':prezto:module:prompt' theme 'skwp'"
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user root

{{ tplfile }}> Add zsh modules for user {{ user }}:
  file.replace:
  - name: /home/{{ user }}/.zprezto/runcoms/zpreztorc
  - backup: False
  - pattern: "( +)('prompt')$"
  - repl: \1\2 \\\n\1'autosuggestions' \\\n\1'syntax-highlighting' \\\n\1'autosuggestions' \\\n\1'syntax-highlighting' \\\n\1'git' \\\n\1'docker' \\\n\1'pacman'
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user {{ user }}

{{ tplfile }}> Add zsh modules for user root:
  file.replace:
  - name: /root/.zprezto/runcoms/zpreztorc
  - backup: False
  - pattern: "( +)('prompt')$"
  - repl: \1\2 \\\n\1'autosuggestions' \\\n\1'syntax-highlighting' \\\n\1'autosuggestions' \\\n\1'syntax-highlighting' \\\n\1'git' \\\n\1'docker' \\\n\1'ssh' \\\n\1'pacman'
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user root

{{ tplfile }}> Configure {{ slspath }} CLOBBER shell option to allow overwriting files for user {{ user }}:
  file.replace:
  - name: /home/{{ user }}/.zprezto/modules/directory/init.zsh
  - pattern: "^(unsetopt CLOBBER)"
  - repl: '# \1'
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user {{ user }}

{{ tplfile }}> Configure history depth for user {{ user }}:
  file.replace:
  - name: /home/{{ user }}/.zprezto/modules/history/init.zsh
  - backup: False
  - pattern: "(_pmh_histsize)=10000$"
  - repl: \1=300000
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user {{ user }}

{{ tplfile }}> Configure history depth for user root:
  file.replace:
  - name: /root/.zprezto/modules/history/init.zsh
  - backup: False
  - pattern: "(_pmh_histsize)=10000$"
  - repl: \1=300000
  - bufsize: file
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user root

{% set symlinks = {'.zlogin':'zlogin', '.zlogout':'zlogout', '.zpreztorc':'zpreztorc', '.zprofile':'zprofile', '.zshenv':'zshenv', '.zshrc':'zshrc.custom' } %}
{% for symlink, target in symlinks.items() %}
{{ tplfile }}> Create symlink /home/{{ user }}/{{ symlink }} Target /home/{{ user }}.zprezto/runcoms/{{ target }}:
  file.symlink:
  - name: /home/{{ user }}/{{ symlink }}
  - target: .zprezto/runcoms/{{ target }}
  - user: {{ user }}
  - group: {{ user }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user {{ user }}

{{ tplfile }}> Create symlink /root/{{ symlink }} Target /root/.zprezto/runcoms/{{ target }}:
  file.symlink:
  - name: /root/{{ symlink }}
  - target: .zprezto/runcoms/{{ target }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }} for user root
{% endfor %}

{{ tplfile }}> Copy saltstack config files:
  file.recurse:
  - name: /etc/salt
  - source: salt://{{ slspath }}/files/etc/salt
  - makedirs: True

{{ tplfile }}> Remove obsolete minion id file:
  file.absent:
  - name: /etc/salt/minion_id

# {{ tplfile }}> Enable saltstack minion service:
#   service.running:
#   - name: salt-minion
#   - enable: True
#   - watch:
#     - file: {{ tplfile }}> Configure saltstack

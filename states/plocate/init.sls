{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

{{ tplfile }}> Configure {{ slspath }}:
  file.managed:
  - name: /etc/updatedb.conf
  - source: salt://{{ slspath }}/files/etc/updatedb.conf
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{{ tplfile }}> Reload {{ slspath }} on config changes:
  cmd.run:
  - name: /usr/bin/updatedb
  - onchanges:
    - file: {{ tplfile }}> Configure {{ slspath }}

#{{ tplfile }}> Enable mlocate on config changes:
#  service.running:
#  - name: sshd
#  - enable: True
#    - watch:
#    - file: {{ tplfile }}> Configure mlocate


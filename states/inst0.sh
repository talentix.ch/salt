#!/usr/bin/env bash

# set -uo pipefail
# trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

#===============================================================================
err_exit() {
  echo
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" | grep -P --color ".+"
  echo "!!! ERROR: $1" | grep -P --color ".+"
  echo "!!!        Aborting script!" | grep -P --color ".+"
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" | grep -P --color ".+"
  echo
  exit 1
}


#===============================================================================
create_backup_mount_path() {
  echo
  echo "=== $FUNCNAME"

  # Done if backup mount path already exists
  if  test -d /backup/ ; then
    echo "    +++ backup mount path already exists: /backup."
    return 0
  fi

  # Create backup mount path
  mkdir /backup && echo "    +++ backup mount path has been created: /backup." || err_exit "Failed to create backup mount path: /backup"

  return 0
}


#===============================================================================
mount_backup_device() {
  echo
  echo "=== $FUNCNAME"

  # # If it is a vm and the backup disk is mapped as a file system then mount the disk provided by the host
  # if dmidecode -s system-manufacturer | grep -q "QEMU"; then
  #   umount -fq /backup
  #   backup_device='/backup'
  #   echo "    +++ We are running in a vm. Mounting backup device $backup_device to the mountpoint /backup"
  #   mount -t virtiofs $backup_device /backup
  #   return
  # fi

  # Done if existing mount is OK
  if mountpoint -q /backup ; then
    backup_device=$(findmnt /backup -n -o source)
    echo "    +++ Target path /backup is already mounted to device: $backup_device"
    read -p "    >>> If this is ok, press ENTER to continue. Otherwise press Ctrl+C to abort."
    return 0
  fi

  # Select device to mount
  select_device "BACKUP"
  echo "    +++ Selected device to be mounted to: /backup: $selected_device_name  $selected_device_size"

  # Mount device
  mount $selected_device_name /backup || err_exit "Failed to mount $selected_device_name to /backup"
  backup_device=$selected_device_name 

  return 0
}


#===============================================================================
check_backup_path() {
  echo
  echo "=== $FUNCNAME"

  test -d /backup/backintime || err_exit "Backup path not found: /backup/backintime. Aborting!"

  echo "    +++ Backup path found: /backup/backintime."

  return 0
}


#===============================================================================
create_backup_symlinks() {
  echo
  echo "=== $FUNCNAME"

  echo
  echo "!!!!!!!!!! TO BE IMPLEMENTED!!!!!!!!!!!!!!!"
  echo
  echo "EXITING"
  exit

  # If symlink already exists, check if it is still OK
  local backup_symlink_target
  if backup_symlink_target=$(readlink /backup/restore-user) ; then
    echo
    echo "    +++ Backup symlink found: $backup_symlink_target."
    echo
    echo "    Enter a selection:"
    echo "       y: to continue with this symlink"
    echo "       n: to recreate the symlink"
    echo "       Ctrl+C: to abort. the script."
    read -p "    >>> y/n: " selection

    # If symlink is still OK
    if test $selection == "y" ; then
      echo "    +++ Continuing with backup symlink: $backup_symlink_target"
      return 0
    fi

    # Delete symlink to let it be re-created
    echo "    +++ Deleting symlink to let it be re-created"
    unlink /backup/restore-user || err_exit "Failed to delete symlink: /backup/restore-user"
    create_backup_symlinks
    return 0
  fi

  # Create symlink

#  shopt -s nullglob
#  arr=(/backup/backintime/talentix/root/config/*)
#
#
#  chk() {
#    local link_target
#    if link_target=$(readlink /backup/restore-user) ; then
#      echo "Link found: $link_target"
#    fi
#  }
#
#
#  select_dir() {
#    echo
#    echo "=== $FUNCNAME"
#
#    menu () {
#      echo ""
#      PS3=">> Select s dir by typing the number (or abort with Ctrl+C): "
#      select item; do
#        # Check the selected menu item number
#        if [ 1 -le "$REPLY" ] && [ "$REPLY" -le $# ]; then
#          echo "The selected dir is: $item"
#          selected_dir_name = $item
#          # selected_dir_array=($item)
#          # selected_dir_name=${selected_dir_array[0]}
#          break;
#        else
#          echo "!! Wrong selection: Select any number from 1-$#"
#        fi
#      done
#    }
#
#    # Get list of potential devices
#    dir_list=(/backup/backintime/talentix/root/config/*)
#
#    # Show menu
#    echo ">> The follwoing dirs have been found:"
#    menu "${dir_list[@]}"
#
#    echo -e "\n>> Selected dir: $selected_dir_name:"
#
#    # If nothing has been selected
#    test -z $selected_dir_name && echo "The selected dir string is empty!"
#  }
#
#
#  select_dir
}


#===============================================================================
select_device() {
  echo
  echo "=== $FUNCNAME"

  test -z "$2" && exclude="undefined" || exclude=$2

  menu () {
    echo
    PS3=">>> Select $device_type-device by typing the number (or abort with Ctrl+C): "
    select item; do
      # Check the selected menu item number
      if [ 1 -le "$REPLY" ] && [ "$REPLY" -le $# ]; then
        # echo "The selected device is: $item"
        selected_device_array=($item)
        selected_device_name=${selected_device_array[0]}
        selected_device_size=${selected_device_array[1]}
        break;
      else
        echo "!!! Wrong selection: Select any number from 1-$#"
      fi
    done
  }

  # Get list of potential devices
  devicelist=$(lsblk -dplnx size -o name,size | grep -Pv "^/dev/(sr|sd)|boot|rpmb|loop|$exclude")
  test -z $devicelist && err_exit "Empty device list!"


  # Create array variable, split by NEWLINES
  SAVEIFS=$IFS
  IFS=$'\n'
  devicelist=($devicelist)
  IFS=$SAVEIFS

  # Show menu
  device_type=$1
  echo "    +++ Select a $1-device - the follwoing devices have been found:"
  menu "${devicelist[@]}"

  echo -e "\n+++ Selected $1-device: ${selected_device_name} ${selected_device_size}:"
  echo -e "+++ -----------------------------------------------"
  lsblk $selected_device_name -pl -o name,pttype,fstype,fsver,parttypename,partlabel,mountpoints,size,fsavail,fsuse%,model

  # If nothing has been selected
  test -z $selected_device_name && err_exit "The selected device string is empty!"

  # echo
  # read -p ">> Press ENTER to continue with partitioning or Ctrl+C to abort."
}


#===============================================================================
select_to_partition() {
  echo
  echo "=== $FUNCNAME"

  # Abort if backup disk has been selected
  # [[ $selected_device_name =~ /dev/vd[a-z] ]] && [[ $selected_device_size =~ [0-9]T$ ]] && err_exit "It looks like you have selected the backup disk!"

  PS3="    >>> Select action by typing the number: "
  actions=("Partition the selected device" "Do not partition the selected device" "Abort")

  select opt in "${actions[@]}"; do
    case $opt in
      "Partition the selected device")
        echo
        echo "    +++ You have selected to partition the selected device: ${selected_device_name} ${selected_device_size}."
        create_partitions
        break
        ;;
      "Do not partition the selected device")
        echo
        echo "    +++ You have selected to skip partitioning. The current partitions are as such:"
        lsblk ${selected_device_name} -pl -o name,pttype,fstype,fsver,parttypename,partlabel,mountpoints,size,fsavail,fsuse%,model
        break
        ;;
      "Abort")
        echo "!!! Abort"
        break
        ;;
      *)
        echo "!!! Invalid option $REPLY"
        ;;
    esac
  done
}


#===============================================================================
create_partitions() {
  echo
  echo "=== $FUNCNAME"
  echo "    +++ Existing mounts will be unmounted and existing partitions will be removed from the selected device ${selected_device_name}!"
  read -p "    >>> Press ENTER to continue or Ctrl+C to abort."

  # umount if mounted
  # echo ">> Unmounting /mnt if already mounted"
  mountpoint -q /mnt && (umount -R /mnt || err_exit "Failed to unmount /mnt!")

  # wipe existing partitions
  wipefs -afq "${selected_device_name}"
  # fdisk -l $selected_device | grep -q "${selected_device}p3" && parted $selected_device rm 3
  # fdisk -l $selected_device | grep -q "${selected_device}p2" && parted $selected_device rm 2
  # fdisk -l $selected_device | grep -q "${selected_device}p1" && parted $selected_device rm 1

  # Create partitions for EFI System and for root.
  parted --script "$selected_device_name" \
    mklabel gpt \
    mkpart ESP fat32 1MiB 1050623s \
    set 1 esp on \
    name 1 '"EFI System"' \
    mkpart Root xfs 1050624s 100% \
    name 2 Root

  sleep 2

  echo
  echo "    +++ Created partitions as shown by fdisk:"
  fdisk -l $selected_device_name | grep -vP "\d+ sectors$|Disk|Units|bytes|label|identi"
  # echo
  # echo "> Device           Start        End    Sectors  Size Type"
  # echo "> /dev/nvme0n1p1    2048    1050623    1048576  512M EFI System"
  # echo "> /dev/nvme0n1p2 9439232 1953523711 1944084480  927G Linux filesystem"
  echo
  echo "    +++ Created partitions as shown by lsblk:"
  echo
  lsblk ${selected_device_name} -pl -o name,pttype,fstype,fsver,parttypename,partlabel,mountpoints,size,fsavail,fsuse%,model
  echo
  # echo
  # echo ">NAME           PTTYPE FSTYPE FSVER PARTTYPENAME     PARTLABEL  MOUNTPOINTS   SIZE FSAVAIL FSUSE% MODEL"
  # echo ">/dev/nvme0n1   gpt                                                         931.5G                Samsung SSD 970 EVO Plus 1TB"
  # echo ">/dev/nvme0n1p1 gpt    vfat   FAT32 EFI System       EFI System /boot         512M  451.1M    12%"
  # echo ">/dev/nvme0n1p2 gpt    xfs          Linux filesystem Root       /             927G  679.7G    27%"
}


#===============================================================================
format_partitions() {
  echo
  echo "=== $FUNCNAME"

  read -p "    >>> Partitions will be formatted! Press ENTER to continue and do the formatting or Ctrl+C to abort."
  
  mountpoint -q /mnt && (umount -R /mnt || err_exit "Failed to unmount /mnt!")
  
  part_boot=$(fdisk -l ${selected_device_name} | grep "EFI System"       | awk '{ print $1 }')
  part_root=$(fdisk -l ${selected_device_name} | grep "Linux filesystem" | awk '{ print $1 }')

  test -z $part_boot && err_exit "Failed to identify boot partition!"
  test -z $part_root && err_exit "Failed to identify root partition!"

  echo
  echo "    +++ mkfs.vfat -F 43 ${part_boot}"
  mkfs.vfat -F 32 "${part_boot}" || err_exit "Failed to make boot filesystem!"
  echo
  echo "    +++ mkfs.xfs -f ${part_root}"
  mkfs.xfs -f "${part_root}" || err_exit "Failed to make root filesystem!"

  echo
  echo "    +++ Formatted partitions as shown by lsblk:"
  echo
  lsblk ${selected_device_name} -pl -o name,pttype,fstype,fsver,parttypename,partlabel,mountpoints,size,fsavail,fsuse%,model

  echo
  read -p "    >>> Press ENTER to continue to mount partitions or Ctrl+C to abort."
}


#===============================================================================
mount_partitions() {
  echo
  echo "=== $FUNCNAME"
  part_root=$(fdisk -l ${selected_device_name} | grep "Linux filesystem" | awk '{ print $1 }')

  test -z $part_boot && err_exit "Failed to identify boot partition!"
  test -z $part_root && err_exit "Failed to identify root partition!"

  # Mount system disks
  echo "    +++ Mounting ${part_root} to: /mnt"
  mount "${part_root}" /mnt || err_exit "Failed to mount partition ${part_root} to /mnt!"
  mkdir /mnt/boot
  echo "    +++ Mounting ${part_boot} to: /mnt/boot"
  mount "${part_boot}" /mnt/boot || err_exit "Failed to mount partition ${part_boot} to /mnt/boot!"

  echo
  echo "    +++ Mounted partitions as shown by lsblk:"
  echo
  lsblk ${selected_device_name} -pl -o name,pttype,fstype,fsver,parttypename,partlabel,mountpoints,size,fsavail,fsuse%,model
  # echo
  # read -p ">> Press ENTER to continue or Ctrl+C to abort."

  echo
  echo "    +++ Mounted partitions as shown by df -h:"
  echo
  df -h

  # Create fstab
  # read -p "Generating /mnt/etc/fstab"
  # genfstab -t PARTUUID /mnt >> /mnt/etc/fstab
}


#===============================================================================
provide_hostname() {
  echo
  echo "=== $FUNCNAME"

  if test -e /mnt/etc/hostname ; then
    echo "    +++ The hostname file already exists: /mnt/etc/hostname! The contents is:"
    cat /mnt/etc/hostname
    echo "    +++ 1. Just press ENTER to keep it."
    echo "    +++ 2. Provide hostname to overwrite /mnt/etc/hostname, then press ENTER."
    read -p "    >>> Or press Ctrl+C to abort: " hostname
    test -z $hostname && hostname=$(cat /mnt/etc/hostname)
    test -z $hostname && err_exit "Hostname must not be empty! The file /mnt/etc/hostname appears to be empty."
  else 
    # Ask for hostname
    read -p "    >>> Provide hostname to be written to /mnt/etc/hostname, then press ENTER to continue or Ctrl+C to abort: " hostname
    test -z $hostname && err_exit "Hostname must not be empty!"
  fi

  # Save it
  echo "$hostname" > /mnt/etc/hostname

  # Show
  echo "    +++ Showing /mnt/etc/hostname:"
  cat /mnt/etc/hostname
  read -p "    >>> Press ENTER to continue or Ctrl+C to abort."
}


#===============================================================================
run_pacstrap() {
  echo
  echo "=== $FUNCNAME"
  read -p "    >>> pacstrap installation will be started. Press ENTER to continue running pacstrap or Ctrl+C to abort."

  ### Install and configure the basic system ###
  pacstrap /mnt base linux linux-firmware openssh git python-tornado python-psutil || err_exit "Failed to run pacstrap!"
  echo
  read -p "    >>> pacstrap installation is done! Press ENTER to continue or Ctrl+C to abort."
}

#===============================================================================
create_fstab() {
  echo
  echo "=== $FUNCNAME"

  read -p "    >>> About to generate /mnt/etc/fstab. Press ENTER to continue or Ctrl+C to abort."
  genfstab -t PARTUUID /mnt >> /mnt/etc/fstab || err_exit "Failed to run genfstab!"
  echo
  echo "    +++ Showing /etc/fstab..."
  cat /mnt/etc/fstab || err_exit "Failed to show fstab!"
  read -p "    >>> Press ENTER to continue or Ctrl+C to abort."

}


#===============================================================================
create_ssh_config() {
  echo
  echo "=== $FUNCNAME"

  # SSH Config
  echo "    +++ Creating /mnt/root/.ssh"
  test -d /mnt/root/.ssh || mkdir -m 700 /mnt/root/.ssh || err_exit "Failed to create /mnt/root/.ssh!"

  # Copy ssh config from backup
  echo "    +++ Copying ssh keys from backup to /mnt/root/.ssh"
  cp -r /backup/restore-root/.ssh /mnt/root/ || err_exit "Failed to copy/create /mnt/root/.ssh!"
  echo "    +++ Showing .ssh folder:"
  ls -ald /mnt/root/.ssh/*
  read -p "    >>> Press ENTER to continue or Ctrl+C to abort."

  # # Unmount backup disk
  # echo "    +++ Unmounting /backup"
  # umount /backup || err_exit "Failed to unmount /backup!"
}


#===============================================================================
configure_saltstack() {
  echo
  echo "=== $FUNCNAME"

  echo "${hostname}" > /mnt/etc/salt/minion_id

  # Save stdout
  # exec 3>&1

  # Redirect all stdout to config file /mnt/etc/salt/minion
  # exec 1> /mnt/etc/salt/minion

  # Restore stdout
  # exec 1>&3

  echo "    +++ Creating saltstack minion config path: /mnt/etc/salt/minion.d"
  mkdir /mnt/etc/salt/minion.d || err_exit "Failed to create /mnt/etc/salt/minion.d!"

  echo "    +++ Creating config file /mnt/etc/salt/minion.d/file-directory.conf"
  cat >/mnt/etc/salt/minion.d/file-directory.conf <<EOF
file_client: local


# Saltstack is configured to run in masterless mode.
# This requires some configs of the salt-master (/etc/salt/master)
# to be added to the saltstack client (/etc/salt/minion).

top_file_merging_strategy: same

file_roots:
  base:
    - /srv/salt/states

pillar_roots:
  base:
    - /srv/salt/pillars
EOF
  read -p ">>> Showing config file. Press ENTER to continue."
  cat /mnt/etc/salt/minion.d/file-directory.conf
  read -p ">>> Press ENTER to continue."

}


#===============================================================================
prepare_installation_as_chroot() {
  echo
  echo "=== $FUNCNAME"

  # Get script
  #-- if ! cp /backup/restore-user/git/talentix/salt-bootstrap/install-as-chroot.sh /mnt/root/ ; then
  #--   echo "    --- Failed to copy /backup/restore-user/git/talentix/salt-bootstrap/install-as-chroot.sh /mnt/root/install-as-chroot.sh /mnt/root/"
  #--   read -p "    >>> Manually copy script install-as-chroot.sh to /mnt/root/ then press ENTER to continue or Ctrl+C to abort."
  #--   chmod +x /mnt/root/install-as-chroot.sh
  #-- fi

  #-- # Set backup_device in script file
  #-- sed -i "s/backup_device/$backup_device/g" /mnt/root/install-as-chroot.sh

  # Unmount backup
  umount /backup

  # Show what to do next...
  printf "\n\n"
  echo "========================================================================"
  echo "Installation as chroot has been prepared."
  echo "========================================================================"
  echo "Issue the following commands:"
  echo "  scp git/talentix/salt-bootstrap/install-as-chroot.sh host:/mnt/root/"
  echo "  arch-chroot /mnt"
  echo "  cd /root"
  echo "  ./install-as-chroot.sh"

return 0
}



#===============================================================================
# MAIN
#===============================================================================
#- create_backup_mount_path
#- mount_backup_device
#- check_backup_path
#- #  create_backup_symlinks
#- select_device "INSTALL" "$backup_device"
#- select_to_partition
#- format_partitions
#- mount_partitions
#- run_pacstrap
#- create_fstab
#- provide_hostname
#- create_ssh_config
configure_saltstack
#- prepare_installation_as_chroot

# arch-chroot /mnt /root/install_as_chroot.sh
#
#echo "The install script: /mnt/root/install.sh has been crerated before switching to arch-chroot."



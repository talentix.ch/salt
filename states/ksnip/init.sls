{% set user = grains.user %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  file.recurse:
  - name: /home/{{ user }}/.config/{{ slspath }}
  - source: salt://{{ slspath }}/files/home/user/config/{{ slspath }}
  - makedirs: True
  - user: {{ user }}
  - group: {{ user }}
  - template: jinja
  - context: 
      user: {{ user }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }}
    - user: {{ user }}
  - unless: test -d /home/{{ user }}/.config/{{ slspath }}

{% set user = grains.user %}

{{ tplfile }}> Create directory for local sources:
  file.directory:
  - name: /home/{{ user }}/.local/sources
  - user: {{ user }}
  - group: {{ user }}
  - makedirs: True

{{ tplfile }}> Clone {{ slspath }}:
  git.cloned:
  - name: https://aur.archlinux.org/{{ slspath }}.git
  - user: {{ user }}
  - target: /home/{{ user }}/.local/sources/{{ slspath }}
  - creates: /home/{{ user }}/.local/sources/{{ slspath }}

{{ tplfile }}> Make and install package {{ slspath }}:
  cmd.run:
  - name: makepkg -sicC --noconfirm
  - runas: {{ user }}
  - cwd: /home/{{ user }}/.local/sources/{{ slspath }}
  - require:
    - {{ tplfile }}> Clone {{ slspath }}
  - onlyif: test -d '/home/{{ user }}/.local/sources/{{ slspath }}'
  - unless: which yay

{{ tplfile }}> Cleanup installation tar ball from {{ slspath }}:
  cmd.run:
  - name: 'rm /home/{{ user }}/.local/sources/{{ slspath }}/{{ slspath }}-*.tar.*'
  - require:
    - {{ tplfile }}> Make and install package {{ slspath }}
  - onlyif: test -f '/home/{{ user }}/.local/sources/{{ slspath }}/{{ slspath }}-*.tar.*'

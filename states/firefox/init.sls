{% set user         = grains.user                                                                                                                    %}
{% set id           = 'qemu' if grains.manufacturer == 'QEMU' else grains.id                                                                         %}
{% set profile_list = {'talentix' : 'kn4p2ffa.default-release', 'qemu' : 'kn4p2ffa.default-release', 'clt-dsk-v-7133' : 'ecsb9og2.default-release' } %}
{% set profile      = profile_list[id]                                                                                                               %}

{{ tplfile }}> Installing {{ slspath }}:
  pkg.installed:
  - name: {{ slspath }}

{{ tplfile }}> Initial cleanup of profile for user {{ user }}:
  file.absent:
  - name: /home/{{ user }}/.mozilla
  - unless: test -d /home/{{ user }}/.mozilla/firefox/{{ profile }}
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{{ tplfile }}> Set firefox as default browser for {{ user }}:
  cmd.run:
  - name: xdg-settings set default-web-browser firefox.desktop
  - runas: {{ user }}
  - unless: xdg-settings get default-web-browser | grep -q "firefox.desktop" 
  - require:
    - {{ tplfile }}> Installing {{ slspath }}

{{ tplfile }}> Configure {{ slspath }} for user {{ user }}:
  cmd.run:
  - name: rsync -a /backup/restore-user/.mozilla /home/{{ user }}/
  - runas: {{ user }}
  - unless: test -d /home/{{ user }}/.mozilla/firefox
  - require:
    - {{ tplfile }}> Installing {{ slspath }}
  - failhard: True

{{ tplfile }}> Installing packages for {{ slspath }}:
  pkg.installed:
 - name: {{ slspath }}
 - pkgs:
   - {{ slspath }}
   - {{ slspath }}-buildx

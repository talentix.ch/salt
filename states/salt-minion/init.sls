{% set master = salt.pillar.get('salt:master', salt.grains.get('master', grains.host)) %}
{% set id = grains.host                                                                %}

{{ tplfile }}> Copy saltstack minion config files:
  file.recurse:
  - name: /etc/salt
  - source: salt://{{ slspath }}/files/etc/salt
  - makedirs: True
  - template: jinja
  - context:
      master: {{ master }}
      id:     {{ id     }}

{{ tplfile }}> Remove obsolete minion id file:
  file.absent:
  - name: /etc/salt/minion_id

# {{ tplfile }}> Enable saltstack minion service, restart on config changes:
#   service.running:
#   - name: salt-minion
#   - enable: True
#   - watch:
#     - file: {{ tplfile }}> Copy saltstack minion config files

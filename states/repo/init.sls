{{ tplfile }}> Create repo list:
  cmd.run:
  - name: curl https://archlinux.org/mirrorlist/all/ | sed -n '/## Switzerland/,/##/p;' | grep -v "##" | sed -E 's/^#//g' > /etc/pacman.d/mirrorlist
  - creates: /etc/pacman.d/mirrorlist
